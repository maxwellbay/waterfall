recorders = data.frame("X"=c(0,0,1,1), "Y" = c(0,1,1,0),
                       row.names=c("A", "B","C","D"))
locs = data.frame("X"=c(.3,.5),"Y"=c(.8,.2))
intensities = data.frame("sine"=sin(0:99*(pi/10))+1.2,
                         "cosine"= .7*cos(0:99*(pi/15))+.9)


dists = matrix(nrow=dim(locs)[1], ncol=dim(recorders)[1],
               dimnames=list(NULL, row.names(recorders)))
for (i in 1:dim(dists)[2]){
  dists[,i]=sqrt((locs$X-recorders$X[i])^2
                 + (locs$Y-recorders$Y[i])^2)}
set.seed(500)
recorded.data = data.frame(jitter(as.matrix(intensities)%*%
                                    as.matrix(exp(-2*dists)),amount=0))


# Obtain data in a matrix
Xoriginal=t(as.matrix(recorded.data))
# Center the data so that the mean of each row is 0
rm=rowMeans(Xoriginal)
X=Xoriginal-matrix(rep(rm, dim(Xoriginal)[1]), nrow=dim(Xoriginal)[2])
# Calculate P
A=X %*% t(X)
E=eigen(A,TRUE)
P=t(E$vectors)
# Find the new data and standard deviations of the principal components
newdata = P %*% X
sdev = sqrt(diag((1/(dim(X)[2]-1)* P %*% A %*% t(P))))


pr=prcomp(recorded.data)
plot(pr)
barplot(pr$sdev/pr$sdev[1])
pr2=prcomp(recorded.data, tol=.1)
plot.ts(pr2$x)
plot.ts(intensities)
plot.ts(recorded.data)
plot.ts(cbind(-1*pr2$x[,1],pr2$x[,2]))