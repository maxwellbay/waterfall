source("src/Waterfall.R")
source("utils/baseB.R")


#funciton that confirms genes meet expression conditions 
meetsCondition = function(mat, gtet = 0, nCells=0){
  condition_mat = ifelse(mat>=gtet,1,0)
  meets_condition = apply(condition_mat,1,sum) >= nCells
  return(meets_condition)

}

#builds association table from the gene attribute table
buildAsscTable = function(path,names_idx,vals_idx){
  mat = read.table(path,header = TRUE)
  xloc = mat[,names_idx]
  gene = mat[,vals_idx]
  names(gene) = xloc
  
  return(gene)
  
}

#finds genename in xloc<->gene association tables
getGene = function(gene,association) {
  return (as.character(names(association)[which(xloctogene%in%gene)]))
}


if (Sys.info()[["sysname"]]=="Darwin"){
  EXPRESSION_PATH = "data/genes.fpkm_table.reordered" #this points to your gene expression file 
  CONVERSION_FILE_PATH = "E:/glps01/GLPS01/David_Cobrinik/113015_DS_HS_mouse_human_rnaseq/human/cuffnorm/genes.attr_table" #this path will need to be changed to point to your genes.attr_table file 
}else{
  EXPRESSION_PATH = "/Waterfall/genes.fpkm_table.reordered" #this points to your gene expression file 
  CONVERSION_FILE_PATH = "data/genes.attr_table" #this path will need to be changed to point to your genes.attr_table file 
}


#Loads transcriptome data
jt = read.table(EXPRESSION_PATH,header = TRUE,sep="\t") #the full Dataset is jt/
rownames(jt) = jt[,1]
jt = jt[,-1]

#XLOC <-> GENE association 
xloctogene = buildAsscTable(CONVERSION_FILE_PATH,1,5)

#remove that bastard of a cell
rmIdx = which(colnames(jt) %in% c("C3_8"))
jt = jt[,-rmIdx]

#Scales gene expression to TPM
#jt = ScaleTPM(jt) #scale to TPM/cell

#log transform
jt = log10(jt)
jt[jt==-Inf] = min(jt[jt>-Inf])


#meets the expression conditions
meets_condition = meetsCondition(jt,gtet=0.1,nCells = 10)
jt_tmp = as.data.frame(jt[meets_condition,])


#Hierarchical Clustering
#Use NULL argument for nClusters for unbiased tree cutting otherwise give positive integer. nClusters = NULL by default. 
grx = Waterfall.Cluster(jt_tmp, nClusters = NULL) 


#Creating color variable from cluster output
jt.cols = grx[,1]

#Removes the side branches by their cluster # (as assigned in Waterfall.Cluster())
colorRmIdx = which(grx[,2] %in% c(6,7,8))

jt = jt[,-colorRmIdx]
jt_tmp = jt_tmp[,-colorRmIdx]
jt.cols = jt.cols[-colorRmIdx]

#Plots in pseudotemporal space
jt.PT = Waterfall.Pseudotime(jt_tmp, angle=0, col = jt.cols, plot_title = " Run1", 
                             nclust = 5, seed = 5,scalePCA = F,mst=FALSE,invert_pt = FALSE, 
                             label=TRUE,threeD_plot = FALSE)

#Sorts TPM data by pseudotime and adds dummy variable "Pseudotime" to gene list
jt.cols = jt.cols[rownames(jt.PT)]
jt_tmp = jt_tmp[,rownames(jt.PT)]
jt_tmp["Pseudotime",] = jt.PT[,1]

jt = jt[,rownames(jt.PT)]
jt["Pseudotime",] = jt.PT[,1]



#example of gene-specific expression visualization with HMM raster
#beta is the exponentiation to return to raw FPKM vals (not log)
#gene_txt parameter takes string and prints as title for our graph
#unit the bin size for hmm cmoputation. getGene() takes the gene 
PTgeneplot(getGene("SYK",xloctogene),jt,col=jt.cols,gene_txt = "SYK",hmm=TRUE,beta = 10,unit = 3,apply_to_graph=TRUE)

#ranks genes by correlation coefficient with pseudotime
pt_cor = ptCor(jt_tmp,jt_tmp["Pseudotime",],beta = 10,method = "spearman")

#computes an hmm raster across a number of input genes (in this case the top 150 [1:150])
#organizes genes by onset time, showing activation at later and later PT times 
rast = cascadingExpression(jt_tmp[c(names(pt_cor$up)[1:150],"Pseudotime"),],
                           beta=10,unit=3,on_prop = .3,avg_mindist_prop = .25,
                           write_img = TRUE,cell_dim = 3.5, asn_table = xloctogene,cex = .65)

