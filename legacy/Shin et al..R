setwd("/Users/rookysh/Desktop/reviewer_codes/M1. Shin et al.")
a <-read.table(file="Single_TPM.txt",header=T)
all <-a
anno <-read.table(file="group0614WholeGenome4.csv",sep=",")

all <-all[,match(anno$V1,colnames(all))]

# Coefficient of variance for a few representative genes in adult neurogenesis
all_bg <-all[which(apply(all,1,function(X){if(sort(X,decreasing=T)[round(ncol(all)/20)]>25){TRUE}else{FALSE}})),]
CV.foo <-function(X){sd(X)/mean(X)}
all_CV <-apply(all_bg,1,CV.foo)
all_CV <-sort(all_CV)
which(names(all_CV)=="Actb")/nrow(all)*100
which(names(all_CV)=="Gapdh")/nrow(all)*100
which(names(all_CV)=="Ubb")/nrow(all)*100
which(names(all_CV)=="Gfap")/nrow(all)*100
which(names(all_CV)=="Sox2")/nrow(all)*100
which(names(all_CV)=="Eomes")/nrow(all)*100
which(names(all_CV)=="Sox11")/nrow(all)*100

c <- cor(all, method="pearson"); d <- dist(t(all)); hr <- hclust(d, method = "ward", members=NULL)

# To determine number of groups
distance_sum <-c()
for (k in 1:11){
    branch=cutree(hr,k=k)
    group_ids <-split(names(branch),branch)
    avg_matrix <-all[,c()]
    all_avg_matrix <-all
    
    for (group.n in 1:length(group_ids)){
        group.idx <-which(colnames(all) %in% group_ids[[group.n]])
        avg_exp <-rowMeans(all[,group.idx])
        all_avg_matrix[,group.idx] <-matrix(rep(avg_exp,length(group.idx)),ncol=length(group.idx),byrow=F)
    }
    
    distance_sum <-c(distance_sum,sum((all-all_avg_matrix)^2))
}
plot(1:length(distance_sum),distance_sum,type="l") # It is a bit subjective, but we chose k=6 for the further analysis

branch=cutree(hr,k=6)
plot(hr,labels=branch)

anno$V4 <-"0"
anno$V4[which(branch=="1")] <-"1"
anno$V4[which(branch=="2")] <-"2"
anno$V4[which(branch=="3")] <-"3"
anno$V4[which(branch=="4")] <-"4"
anno$V4[which(branch=="5")] <-"5"
anno$V4[which(branch=="6")] <-"A"

col_sel <-c("#4882C3","#F26A6A","#13751B","#FF6A00","#E2CF00","#980B0B")
all.col <-rep("#BFBFBF30",length(branch))
all.col[which(anno$V4=="1")] <-"#4882C3" #1 blue
all.col[which(anno$V4=="2")] <-"#F26A6A" #2 salmon
all.col[which(anno$V4=="3")] <-"#13751B" #3 darkgreen
all.col[which(anno$V4=="4")] <-"#FF6A00" #4 orange
all.col[which(anno$V4=="5")] <-"#E2CF00" #5 yellow
all.col[which(anno$V4=="A")] <-"#980B0B" #Ap darkred
names(all.col) <-anno$V1

pca <-prcomp(t(all))
plot(pca$x[,1:2],col=all.col,pch=19,cex=2)

d <- dist(t(all))
r <- sammon(d,niter=500000,tol=1e-500)
x <- r$points
library(deldir)
sammon.dd <-deldir(x[,1],x[,2])
plot(tile.list(sammon.dd),fillcol=all.col,showpoints=FALSE) # As opposed to the Voronoi plot without A a few rows below, this voronoi plot shows a skewed distribution, presumably due to SA.
####################

plot(pca$x[,1:2],col=paste0(all.col,99),cex=scale_row.foo(as.numeric(all[grep("Fabp7",rownames(all)),]))*8+.3,pch=19)
plot(pca$x[,1:2],col=paste0(all.col,99),cex=scale_row.foo(as.numeric(all[grep("Gfap",rownames(all)),]))*8+.3,pch=19)
plot(pca$x[,1:2],col=paste0(all.col,99),cex=scale_row.foo(as.numeric(all[grep("Sox11",rownames(all)),]))*8+.3,pch=19)
plot(pca$x[,1:2],col=paste0(all.col,99),cex=scale_row.foo(as.numeric(all[grep("Eomes",rownames(all)),]))*8+.3,pch=19)

mst.of.classification(all,k=6,col=paste0(all.col,""),seed=1)

#####################

anno <-anno[-which(anno$V4=="A"),]
anno <-anno[anno$V1 %notin% c("C11","C16","C25","C28","C13"),] # Removing S0; far left located; please see the Supplementary Methods II.1.(3) Additional trajectories
anno <-anno[anno$V1 %notin% c("C48","C44","C138"),] # Removing outliers

all <-all[,match(anno$V1,colnames(all))]
all.col <-all.col[match(anno$V1,names(all.col))]

pca <-prcomp(t(all))
plot(pca$x[,1:2],col=all.col,pch=19,cex=2)

d <- dist(t(all))
r <- sammon(d,niter=500000,tol=1e-500)
x <- r$points
library(deldir)
sammon.dd <-deldir(x[,1],x[,2])
plot(tile.list(sammon.dd),fillcol=all.col,showpoints=FALSE) # As opposed to the previous Voronoi plot with A a few lines above, this voronoi plot shows a more relaxed distribution, presumably due to the absence of a distinct group SA.

# determine k means
distance_sum <-c()
for (k in 1:8){
    r <-kmeans(pca$x[,1:2],k)
    distance_sum <-c(distance_sum,r$tot.withinss)
}
plot(1:8,distance_sum,type="l") # It is a bit subjective, but we chose k=4 in this case.

mst.of.classification(all,k=4,col=paste0(all.col,""),seed=1)
pseudotime.df <-pseudotimeprog.foo(all,k=4,seed=1,color=all.col)

all.col <-all.col[match(rownames(pseudotime.df),names(all.col))]
all <-all[,match(rownames(pseudotime.df),colnames(all))]

# The following function generates pseudotime gene expression profile with HMM predicted heatmap on/high and off/low states. We merged the two graphs in graphical tools!
pseudotime.foo("Gfap",all=all,all.col=all.col,span=.75)
pseudotime.foo("Sox11",all=all,all.col=all.col,span=.75)
pseudotime.foo("^Sox2$",all=all,all.col=all.col,span=.75)
pseudotime.foo("Eomes",all=all,all.col=all.col,span=.75)
pseudotime.foo("Fabp7",all=all,all.col=all.col,span=.75)
pseudotime.foo("Apoe",all=all,all.col=all.col,span=.75)
pseudotime.foo("CFP",all=all,all.col=all.col,span=.75)
pseudotime.foo("Pcna",all=all,all.col=all.col,span=.75)
pseudotime.foo("Hmgn2",all=all,all.col=all.col,span=.75)

library(KEGGREST)
goi.clut <-unlist(strsplit(keggGet("mmu00190")[[1]]$GENE,split="; "))# mmu00190: oxidative phosphorylation
goi <-goi.clut[c(1:(length(goi.clut)/3))*3-1]
goi_group <-goi.clut[c(1:(length(goi.clut)/3))*3]

complex1.idx <-grep("NADH dehydrogenase",goi_group)
complex2.idx <-grep("succinate dehydrogenase complex",goi_group)
complex3.idx <-grep("ubiquinol-cytochrome c reductase",goi_group)
complex4.idx <-grep("cytochrome c oxidase",goi_group)
complex5.idx <-grep("ATP synthase",goi_group)

comI.df <-data.frame(comI=goi[complex1.idx]);comII.df <-data.frame(comII=goi[complex2.idx]);comIII.df <-data.frame(comIII=goi[complex3.idx]);comIV.df <-data.frame(comIV=goi[complex4.idx]);comV.df <-data.frame(comV=goi[complex5.idx])
oxpho.df <-as.data.frame(c(as.character(comI.df[,1]),as.character(comII.df[,1]),as.character(comIII.df[,1]),as.character(comIV.df[,1]),as.character(comV.df[,1])))
colnames(oxpho.df) <-"com1to5"

mit.df <-comII.df

for (i in 1:nrow(mit.df)){
tryCatch({
        pseudotime.foo(mit.df[i,1],all=all,all.col=all.col,span=.75)
    
    if (i==7) stop("Urgh")
}, error=function(e){})
}

sin_scale2.foo <-function(m){
    scale_row.foo <-function(X){
        medX <-max(median(X),200)
        maxX <-max(max(X),2*200)
        Y <-rep(0,length(X))
        l.idx <-which(X>=medX)
        s.idx <-which(X<medX)
        
        Y[s.idx] <- 0.5*sin((X[s.idx]-medX)/medX*pi/2)+.5
        Y[l.idx] <- 0.5*sin((X[l.idx]-medX)/(max(X)-medX)*pi/2)+.5
        
        return(Y)
    }
    t(apply(m,1,scale_row.foo))
}

pca <- prcomp(as.data.frame(t(all)))
scatter3d(as.numeric(pca$x[,1]),as.numeric(pca$x[,2]),as.numeric(pca$x[,3]),point.col=all.col,sphere.size=3,radius=3,surface.alpha=0,fill=F,fogtype="none",residuals=F,square.col="red",xlab="PC1",ylab="PC2",zlab="PC3")


##### Test monocle ######
library(monocle)

all <-a
anno <-read.table(file="group0614WholeGenome4.csv",sep=",")

all <-all[,match(anno$V1,colnames(all))]
HSMM_expr_matrix <-as.matrix(all)

pheno.data.df <- data.frame(type=as.character(anno$V3))
rownames(pheno.data.df) <- as.character(anno$V1)
pd <- new('AnnotatedDataFrame', data = pheno.data.df)

#HSMM <- new('CellDataSet', exprs = HSMM_expr_matrix, phenoData = pd) # This is an old code that never works...
HSMM <- newCellDataSet(HSMM_expr_matrix, phenoData = pd)
HSMM <- detectGenes(HSMM, min_expr = 0.1)
print(head(pData(HSMM)))

expressed.genes <- row.names(subset(fData(HSMM), num_cells_expressed >= 10))
ordering.genes <- row.names(HSMM)[1:1000] # We used 1000 highest expressed genes for the downstream analyses
HSMM <- setOrderingFilter(HSMM, ordering.genes) # Set list of genes for ordering
HSMM <- reduceDimension(HSMM, use_irlba = F) # Reduce dimensionality
HSMM <- orderCells(HSMM, num_paths = 2, reverse=F) # Order cells

plot_spanning_tree(HSMM, color_by="type")
my_genes <- c("Fabp7", "Gfap", "Eomes", "Sox11")
cds_subset <- HSMM[my_genes,]
plot_genes_in_pseudotime(cds_subset, color_by="type",cell_size=1.5)

write.table(pData(HSMM)[order(pData(HSMM)[,3]),],file="Monocle.txt",quote=F,sep="\t")
all <-all[,match(rownames(pData(HSMM)[order(pData(HSMM)[,3]),]),colnames(all))]
write.table(all,file="Monocle_TPM.txt",sep="\t",quote=F)

##### Test monocle without SA######
# This is an additional testing Monocle without SA to help it better appreciate the dataset; and compare the total distance
library(monocle)

all <-a
anno <-anno[anno$V1 %in% rownames(pseudotime.df),]
all <-all[,match(anno$V1,colnames(all))] # remove SA

HSMM_expr_matrix <-as.matrix(all)
pheno.data.df <- data.frame(type=as.character(anno$V3))
rownames(pheno.data.df) <- as.character(anno$V1)
pd <- new('AnnotatedDataFrame', data = pheno.data.df)

HSMM <- newCellDataSet(HSMM_expr_matrix, phenoData = pd)
HSMM <- detectGenes(HSMM, min_expr = 0.1)
print(head(pData(HSMM)))

expressed.genes <- row.names(subset(fData(HSMM), num_cells_expressed >= 10))
ordering.genes <- row.names(HSMM)[1:1000]
HSMM <- setOrderingFilter(HSMM, ordering.genes) # Set list of genes for ordering
HSMM <- reduceDimension(HSMM, use_irlba = F) # Reduce dimensionality
HSMM <- orderCells(HSMM, num_paths = 2, reverse=F) # Order cells

plot_spanning_tree(HSMM, color_by="type")
my_genes <- c("Fabp7", "Gfap", "Eomes", "Sox11")
cds_subset <- HSMM[my_genes,]
plot_genes_in_pseudotime(cds_subset, color_by="type",cell_size=1.5)

write.table(pData(HSMM)[order(pData(HSMM)[,3]),],file="Monocle.txt",quote=F,sep="\t")
all <-all[,match(rownames(pData(HSMM)[order(pData(HSMM)[,3]),]),colnames(all))]
write.table(all,file="Monocle_TPM.txt",sep="\t",quote=F)

# Quantitative comparison between Waterfall and Monocle by total distance of process
#### Permutation ####
library(foreach); library(doMC)
registerDoMC(1)

# Total distance by Waterfall (without SA)
all <-a[,match(rownames(pseudotime.df),colnames(a))]
dist_tpm.mtx <-as.matrix(dist(t(all)))
traveling.mtx <-matrix(c(0,rep(c(1,rep(0,ncol(dist_tpm.mtx))),ncol(dist_tpm.mtx)-1)),byrow=T,ncol=ncol(dist_tpm.mtx))
distance_Waterfall <-c(sum(dist_tpm.mtx * traveling.mtx))

# Total distance by Monocle (without SA)
all <-a[,match(rownames(pData(HSMM))[order(pData(HSMM)$Pseudotime)],colnames(a))]
dist_tpm.mtx <-as.matrix(dist(t(all)))
traveling.mtx <-matrix(c(0,rep(c(1,rep(0,ncol(dist_tpm.mtx))),ncol(dist_tpm.mtx)-1)),byrow=T,ncol=ncol(dist_tpm.mtx))
distance_Monocle <-c(sum(dist_tpm.mtx * traveling.mtx))

# Permutation for distribution
n_permut=0
distance_permuted <-c()

while (n_permut<100){
    n_permut <-n_permut+1
    distance_permuted <-c(distance_permuted,unlist(foreach(i=1:1000000) %dopar% {
        traveling.mtx <-traveling.mtx[sample(1:ncol(traveling.mtx),ncol(traveling.mtx),replace=FALSE),]
        sum(dist_tpm.mtx * traveling.mtx)
    }))
    print(min(distance_permuted))
}

pnorm(distance_Waterfall, mean = mean(distance_permuted), sd = sd(distance_permuted), lower.tail = TRUE)
pnorm(distance_Monocle, mean = mean(distance_permuted), sd = sd(distance_permuted), lower.tail = TRUE)

d <-density(c(distance_Waterfall,distance_Monocle,distance_permuted))
plot(d,xlim=c(min(c(distance_Waterfall,distance_Monocle,distance_permuted)),max(distance_permuted)))
polygon(d, col="black", border="black")
points(distance_Waterfall,0,col="red")
points(distance_Monocle,0,col="green")