source("src/Waterfall.R")
'%notin%' <- function(x,y)!('%in%'(x,y))

jt = read.table("data/Jaden_TPM_NSC.txt") #the full Dataset is jt

jt = ScaleTPM(jt)            #the full Dataset is jt

grx = Waterfall.Cluster(jt, nClusters = NULL)
#Color S0: Purple
#S0 = c("C11","C16","C25","C28","C13")
#grx[S0,1] = "purple"
jt.cols = grx[,1] #Includes SA

S1.id = names(grx[grx[,2]==1,2])
S2.id = names(grx[grx[,2]==2,2])
S3.id = names(grx[grx[,2]==3,2])
S4.id = names(grx[grx[,2]==4,2])
S5.id = names(grx[grx[,2]==5,2])
SAn.id = names(grx[grx[,2]==6,2])
SAa.id = names(grx[grx[,2]==7,2])


jt = jt[,grx[,2] %notin% 6:7] # 3,4,5, #remove SA, keep all others (S0 in S1)
jt.cols = grx[,1][grx[,2] %notin% 6:7] #3,4,5

#jt.cols[c(S1.id,S2.id,S3.id)] = as.character(jt.cols[S1.id[1]])

jt.all = jt

#note, flipping graph so that Pseudotime is in same direction
#jt = jt[-1,]
jt.PT = Waterfall.Pseudotime(jt, angle=0, col = jt.cols, plot_title = "", nclust = 5, seed = 5, invert_pt = TRUE,scalePCA = F,lines=TRUE,mst=F,threeD_plot = TRUE)

#SortJaden by PT, create a dummy gene called "PT"
jt.cols = jt.cols[rownames(jt.PT)]
jt = jt[,rownames(jt.PT)]
jt["Pseudotime",] = jt.PT[,1]





