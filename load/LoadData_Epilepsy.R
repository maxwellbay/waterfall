source("src/Waterfall.R")
source("src/HyperGeoTerms.R")
source("src/heatmap3.R")
source("src/heatmap3_custom.R")
source("src/CMap.R")
library(data.table)
library(plyr)
library(reshape2)
library(rgl)
library(Barnard)

#for 3d plot download: http://xquartz.macosforge.org/trac/wiki

#gao
nt = read.table("data/bhw040supp_table2.txt",comment.char = "!")
nt = nt[-which(nt[,1] == "#N/A"),]
nt = rmDupGenes(nt,blend_function = "sum")
nt = ScaleTPM(nt)

#shin
ejt = jt
ejt = ScaleTPM(ejt)

#epilepsy
source("load/LoadData_EpilepsyItter.R")


#mature DG neurons
yt = read.table("data/GSE74985_genes.fpkm_tracking", header = TRUE,row.names = 1)
yt = yt[,c(4,grep("FPKM",colnames(yt)))]
yt = yt[,c(1,grep("dg",colnames(yt)))]
yt = rmDupGenes(yt,blend_function = "sum")
yt = ScaleTPM(yt)
#yt = yt[,grep("dg",colnames(yt))]


#both_genes = Reduce(intersect, list(rownames(nt),rownames(ejt),rownames(et)))
#both_genes = Reduce(intersect, list(rownames(nt),rownames(ejt),rownames(yt)))
both_genes = Reduce(intersect, list(rownames(nt),rownames(ejt),rownames(yt),rownames(et)))

nt.sub = nt[both_genes,]
ejt.sub = ejt[both_genes,]
et.sub = et[both_genes,]
yt.sub = yt[both_genes,]
njt = cbind(ejt.sub,nt.sub,yt.sub,et.sub)

#old colorations jt & nt
njt.cols = rep("black",ncol(njt));names(njt.cols) = colnames(njt)
njt.cols[names(jt.cols[names(jt.cols)%in%names(njt.cols)])] = jt.cols[names(jt.cols)%in%names(njt.cols)]
njt.cols[grep("dg",names(njt.cols))] = "green"
njt.cols[grep("I",names(njt.cols))] = "purple"
njt.cols[grep("N",names(njt.cols))] = "cyan"
njt.cols[grep("X",names(njt.cols))] = "grey"

color_vect = njt.cols

Waterfall.Cluster(njt,nClusters = 2,cell.cols = color_vect)
njt.PT =  Waterfall.Pseudotime(njt, angle=180, col = njt.cols, plot_title = " CFP+DCX", nclust = 9, seed = 9, invert_pt = T,scalePCA = F,lines=F,mst=F,threeD_plot=TRUE,rndY = TRUE,label = TRUE)
njt = njt[,rownames(njt.PT)]
njt["Pseudotime",] = njt.PT[,1]
njt.cols = njt.cols[colnames(njt)]

c = corGenes(njt,njt["Pseudotime",])
pca_c = Waterfall.PCA(njt,cols = njt.cols,twoDcomp = c(1,2),threeDcomp = c(1,2,3),threeD_plot = TRUE)

cellnums = c(56,22,43,81,41,62,79,21,73,66,57,80)
rm_cells = paste0(rep("sample",length(cellnums)),cellnums)

nt.post = colnames(nt)[!colnames(nt) %in% rm_cells]

fc = foldChange(njt[,c(nt.post,colnames(yt))],nt.post,colnames(yt),mean_thresh = 1)
DIM = 20
fcUP = rownames(fc)[1:DIM]
fcDN = rownames(fc)[(nrow(fc)-(DIM-1)):nrow(fc)]

dfGenes = c(fcUP,fcDN)
njt.df = njt[dfGenes,-which(colnames(njt) %in% c(rm_cells,colnames(jt)))]

grx.df = Waterfall.Cluster(njt.df)

grp1 = rownames(grx.df)[grx.df[,2]=="1"]
grp2 = rownames(grx.df)[grx.df[,2]=="2"]
grp3 = rownames(grx.df)[grx.df[,2]=="3"]
grp4 = rownames(grx.df)[grx.df[,2]=="4"]

njt.df.grp = njt.df[dfGenes,-which(!colnames(njt.df) %in% c(grp1))]
grx.df.grp = Waterfall.Cluster(njt.df.grp,nClusters = 2)

groups = grx.df.grp[,2]; groups = groups[grep("DG",names(groups))]
mature = names(groups[groups=="2"])
mature_i = mature[grep("I",mature)]
mature_n = mature[grep("N",mature)]

immature = names(groups[groups=="1"])
immature_i = immature[grep("I",immature)]
immature_n = immature[grep("N",immature)]

ines = names(groups); ines[grep("N",ines)] = "Non"; ines[grep("I",ines)] = "Ictal"
mnes = as.character(groups); mnes[mnes=="2"] = "Mature"; mnes[mnes=="1"] = "Immature"
tbl = table(ines,mnes)

barnard.test(tbl[1,1],tbl[2,1],tbl[1,2],tbl[2,2],pooled = TRUE)

maturity = paste0(as.character(ifelse(groups=="2","M","I")),"_",1:length(groups))
et.gps = et[,names(groups)];colnames(et.gps) = maturity
new_genes = signatureAll(et.gps,colnames(et.gps)[grep("M",colnames(et.gps))])
new_genes.mature.sub = new_genes[(new_genes$meanA>100 | new_genes$meanB>100) & new_genes$pval<.01 & (new_genes$log2FC>4 | new_genes$log2FC<1/4),]

#Egr1, Myc, Arc

i = c("#c91900","#2B788A"); names(i) = c("Ictal","Non") 
m = c("black","#f5f5b8"); names(m) = c("Immature","Mature")


clab = as.matrix(rbind(i[ines],m[mnes]))
clab = as.matrix(clab)

myclust=function(c) {hclust(c,method = "ward", members=NULL)}

heatmap.3(log2(et.gps[c("Fos","Jun","Egr1"),]+1),hclustfun=myclust, ColSideColors = t(clab),ColSideColorsSize = 2.5,col = colorRampPalette(rev(brewer.pal(n = 10, name = "RdYlBu")))(10),KeyValueName = "Log2(TPM+1)")
legend(x = 1e-15,y = .9,legend=c("Immature","Mature","Ictal","Non-Ictal"),
       fill=c("black","#f5f5b8","#c91900","#2B788A"), border=TRUE, bty="n", y.intersp = 0.7, cex=0.7)

heatmap.3(log2(et.gps[rownames(new_genes.mature.sub),]+1),hclustfun=myclust, ColSideColors = t(clab),ColSideColorsSize = 2.5,col = colorRampPalette(rev(brewer.pal(n = 10, name = "RdYlBu")))(10),KeyValueName = "log2(TPM+1)")
legend(x = 1e-15,y = .9,legend=c("Immature","Mature","Ictal","Non-Ictal"),
       fill=c("black","#f5f5b8","#c91900","#2B788A"), border=TRUE, bty="n", y.intersp = 0.7, cex=0.7)

heatmap.3.custom(et.gps[dfGenes,],ColSideColors = t(clab),hclustfun=myclust, 
          ColSideColorsSize = 2.5,col = colorRampPalette(rev(brewer.pal(n = 10, name = "RdYlBu")))(10),
          KeyValueName = "log2(TPM+1)")
legend(x = 1e-15,y = .9,legend=c("Immature","Mature","Ictal","Non-Ictal"),
       fill=c("black","#f5f5b8","#c91900","#2B788A"), border=TRUE, bty="n", y.intersp = 0.7, cex=0.7)

#Barnard Exact Test (optimal for <<n 2x2)
barnard.test(tbl[1,1],tbl[1,2],tbl[2,1],tbl[2,2],pooled = TRUE)

#connectivity map 
epTPM.dg = epTPM[,c(grep("DG_I",colnames(epTPM)),grep("DG_N",colnames(epTPM)))]
ict.sig = signatureAll(epTPM.dg,colnames(epTPM.dg)[grep("I",colnames(epTPM.dg))])
ictal_signature = ict.sig[ict.sig$log2FC>=2 & ict.sig$pval<=.05 & ict.sig$meanA>=50,]
nonictal_signature = ict.sig[ict.sig$log2FC<=1/2 & ict.sig$pval<=.05 & ict.sig$meanB>=50,]

up_ictal = capitalize(tolower(rownames(ictal_signature)))
dn_ictal = capitalize(tolower(rownames(nonictal_signature)))

toAffy(dn_ictal,up_ictal)

trans_sig_drug_priority = priorityCmap("~/Desktop/permutedResults150692.xls",weightMat = c(5,2,5),betaMat = c(1/4,3,6),reordered_parent = "/Users/maxwellbay/Desktop")

var_genes = apply(epTPM.dg,1,var)
var_genes = var_genes[order(var_genes,decreasing = TRUE)]
nm.var_genes = names(var_genes)[1:2000]

epNet = networkAnalysisBlock(t(epTPM.dg[nm.var_genes,]), minModuleSize = 10, softPower = 9,module_genes_path = "~/Desktop/ep2Modules.csv", block_save_dir = "~/Desktop/epDat", association_names = TRUE,plotMatrix = FALSE)


#n for pops 
num_i = length(mature_i) + length(immature_i)
num_n = length(mature_n) + length(immature_n)


#probe for death signature 
avg_exp = as.data.frame(matrix(0,ncol = length(dates_dict),nrow = nrow(et.nms_only)));rownames(avg_exp) = rownames(et.nms_only); colnames(avg_exp) = names(dates_dict)
for (date in names(dates_dict)){
  date = as.character(date)
  cells = as.character(dates_dict[[date]])
  exp = apply(et.nms_only[,cells],1,mean)
  avg_exp[,date] = exp
}


myclust=function(c) {hclust(c,method = "ward", members=NULL)}
heatmap.3.custom(avg_exp[c(up_ictal,dn_ictal),],hclustfun=myclust
                 ,col = colorRampPalette(rev(brewer.pal(n = 10, name = "RdYlBu")))(10),
                 KeyValueName = "log2(TPM+1)") # just DE genes
heatmap.3.custom(avg_exp[nm.var_genes,],hclustfun=myclust
                 ,col = colorRampPalette(rev(brewer.pal(n = 10, name = "RdYlBu")))(10),
                 KeyValueName = "log2(TPM+1)")# top 2k most variable genes


#inter-ictal only 
avg_exp.i = as.data.frame(matrix(0,ncol = length(dates_dict),nrow = nrow(et.nms_only.I)));rownames(avg_exp.i) = rownames(et.nms_only.I); colnames(avg_exp.i) = names(dates_dict)
for (date in names(dates_dict)){
  date = as.character(date)
  cells = as.character(dates_dict[[date]])
  if (length(cells) < 2) {
    exp = et.nms_only.I[,colnames(et.nms_only.I) %in% cells]
  }else{
    exp = apply(et.nms_only.I[,colnames(et.nms_only.I) %in% cells],1,mean)
  }
  avg_exp.i[,date] = exp
}



#all genes
out = Waterfall.Cluster(avg_exp.i)

#heatmap
heatmap.3.custom(avg_exp.i[capitalize(tolower(nm.var_genes)),],hclustfun=myclust
                 ,col = colorRampPalette(rev(brewer.pal(n = 10, name = "RdYlBu")))(10),
                 KeyValueName = "log2(TPM+1)")# top 2k most variable genes

#heatmap DE
heatmap.3.custom(avg_exp.i[c(up_ictal),],hclustfun=myclust
                 ,col = colorRampPalette(rev(brewer.pal(n = 10, name = "RdYlBu")))(10),
                 KeyValueName = "log2(TPM+1)")


i = c("#c91900","#2B788A"); names(i) = c("I","N") 
d = c("red","blue","orange","green","purple","black"); names(d) = c("11/1/16","12/16/16","9/8/15","10/18/16","9/17/15","5/5/16")


clab = as.matrix(rbind(i[as.character(tmp[colnames(et.nms_only),"ICTAL"])],d[as.character(tmp[colnames(et.nms_only),"DATE"])]))
clab = as.matrix(clab)

#each cell 
heatmap.3.custom(et[up_ictal,],ColSideColors = t(clab),hclustfun=myclust, 
                 ColSideColorsSize = 2.5,col = colorRampPalette(rev(brewer.pal(n = 10, name = "RdYlBu")))(10),
                 KeyValueName = "log2(TPM+1)")
legend(x = .000001,y = .8,legend=c("11/1/16","12/16/16","9/8/15","10/18/16","9/17/15","5/5/16","Ictal","Non-Ictal"),
       fill=c("red","blue","orange","green","purple","black","#c91900","#2B788A"), border=TRUE, bty="n", y.intersp = 0.7, cex=0.7)

#each cell mt
heatmap.3.custom(et[c("Cox15","Ndufa4","Ndufs5","Atp5c1",rownames(et)[grep("Mt-",rownames(et.nms_only))]),],ColSideColors = t(clab),hclustfun=myclust, 
                 ColSideColorsSize = 2.5,col = colorRampPalette(rev(brewer.pal(n = 10, name = "RdYlBu")))(10),
                 KeyValueName = "log2(TPM+1)")
legend(x = .000001,y = .8,legend=c("11/1/16","12/16/16","9/8/15","10/18/16","9/17/15","5/5/16","Ictal","Non-Ictal"),
       fill=c("red","blue","orange","green","purple","black","#c91900","#2B788A"), border=TRUE, bty="n", y.intersp = 0.7, cex=0.7)
#log2(mat+1) distance
heatmap.3(log2(et[c("Cox15","Ndufa4","Ndufs5","Atp5c1",rownames(et)[grep("Mt-",rownames(et.nms_only))]),]+1),ColSideColors = t(clab),hclustfun=myclust, 
                 ColSideColorsSize = 2.5,col = colorRampPalette(rev(brewer.pal(n = 10, name = "RdYlBu")))(10),
                 KeyValueName = "log2(TPM+1)")
legend(x = .000001,y = .8,legend=c("11/1/16","12/16/16","9/8/15","10/18/16","9/17/15","5/5/16","Ictal","Non-Ictal"),
       fill=c("red","blue","orange","green","purple","black","#c91900","#2B788A"), border=TRUE, bty="n", y.intersp = 0.7, cex=0.7)




<<<<<<< HEAD
<<<<<<< HEAD
pca = Waterfall.PCA(njt,cols = njt.cols,twoDcomp = c(1,2),threeDcomp = c(1,2,3),threeD_plot = TRUE)
=======
=======
#table 
included = c(mature,immature)
included = unlist(lapply(strsplit(included,"_"),function(l)l[3]))
dts.dg.included = dts[included]

dates_dict.included = list()
for (cell in names(dts.dg.included)){
  date = dts.dg.included[cell]
  if(!date %in% names(dates_dict.included)){
    dates_dict.included[[date]] = list()
    dates_dict.included[[date]][["Ictal"]] = 0
    dates_dict.included[[date]][["nonIctal"]] = 0
    dates_dict.included[[date]][["cells"]] = cell
>>>>>>> 09d2c253d0f5f15834553e91ba3f88d66d9773d6
  
    if(as.character(tmp[cell,"ICTAL"]) == "I") dates_dict.included[[date]][["Ictal"]] = dates_dict.included[[date]][["Ictal"]] + 1
    if(as.character(tmp[cell,"ICTAL"]) == "N") dates_dict.included[[date]][["nonIctal"]] = dates_dict.included[[date]][["nonIctal"]] + 1
    
  }else{
    dates_dict.included[[date]][["cells"]] = c(dates_dict.included[[date]][["cells"]],cell)
    if(as.character(tmp[cell,"ICTAL"]) == "I") dates_dict.included[[date]][["Ictal"]] = dates_dict.included[[date]][["Ictal"]] + 1
    if(as.character(tmp[cell,"ICTAL"]) == "N") dates_dict.included[[date]][["nonIctal"]] = dates_dict.included[[date]][["nonIctal"]] + 1
    
  }
}
<<<<<<< HEAD
>>>>>>> b7068cc73cb3cd1c6af8a2ae183d18677fe72303
=======

#DE expression after exclusions
et.nms_only.included = et.nms_only[,included]

ict_cells = included[as.character(tmp[included,"ICTAL"]) == "I"]
nict_cells = included[as.character(tmp[included,"ICTAL"]) == "N"]

#rm 11 and 12
ict_cells.keep = ict_cells[!ict_cells %in% c(dates_dict.included$`11/1/16`$cells,dates_dict.included$`12/16/16`$cells)]
nict_cells.keep = nict_cells[!nict_cells %in% c(dates_dict.included$`11/1/16`$cells,dates_dict.included$`12/16/16`$cells)]

ict.sig = signatureAll(et.nms_only[,c(ict_cells.keep,nict_cells.keep)],ict_cells.keep)
ictal_signature = ict.sig[ict.sig$log2FC>=2 & ict.sig$pval<=.05 & ict.sig$meanA>=50,]
nonictal_signature = ict.sig[ict.sig$log2FC<=1/2 & ict.sig$pval<=.05 & ict.sig$meanB>=50,]

write.csv(ictal_signature,file = "~/Desktop/up_ictal-11&12rm.csv")
write.csv(dn_ictal,file = "~/Desktop/dn_ictal-11&12rm.csv")

up_ictal = capitalize(tolower(rownames(ictal_signature)))
dn_ictal = capitalize(tolower(rownames(nonictal_signature)))

toAffy(dn_ictal,up_ictal)
#run through c-map
trans_sig_drug_priority = priorityCmap("~/Desktop/usc/grants/CUREepilepsy-4.2017/rm11-12/permutedResults151132.xls",weightMat = c(5,2,5),betaMat = c(1/4,3,6),reordered_parent = "/Users/maxwellbay/Desktop/usc/grants/CUREepilepsy-4.2017/rm11-12")

###
#rm 11
ict_cells.keep = ict_cells[!ict_cells %in% c(dates_dict.included$`11/1/16`$cells)]
nict_cells.keep = nict_cells[!nict_cells %in% c(dates_dict.included$`11/1/16`$cells)]

ict.sig = signatureAll(et.nms_only[,c(ict_cells.keep,nict_cells.keep)],ict_cells.keep)
ictal_signature = ict.sig[ict.sig$log2FC>=2 & ict.sig$pval<=.05 & ict.sig$meanA>=50,]
nonictal_signature = ict.sig[ict.sig$log2FC<=1/2 & ict.sig$pval<=.05 & ict.sig$meanB>=50,]

write.csv(ictal_signature,file = "~/Desktop/usc/grants/CUREepilepsy-4.2017/rm11/up_ictal-11rm.csv")
write.csv(nonictal_signature,file = "~/Desktop/usc/grants/CUREepilepsy-4.2017/rm11/dn_ictal-11rm.csv")

up_ictal = capitalize(tolower(rownames(ictal_signature)))
dn_ictal = capitalize(tolower(rownames(nonictal_signature)))
toAffy(dn_ictal,up_ictal,outbase = "~/Desktop/usc/grants/CUREepilepsy-4.2017/rm11")
trans_sig_drug_priority = priorityCmap("~/Desktop/usc/grants/CUREepilepsy-4.2017/rm11/permutedResults151133.xls",weightMat = c(5,2,5),betaMat = c(1/4,3,6),reordered_parent = "/Users/maxwellbay/Desktop/usc/grants/CUREepilepsy-4.2017/rm11")


###
#rm 10-11
ict_cells.keep = ict_cells[!ict_cells %in% c(dates_dict.included$`11/1/16`$cells,dates_dict.included$`10/18/16`$cells)]
nict_cells.keep = nict_cells[!nict_cells %in% c(dates_dict.included$`11/1/16`$cells,dates_dict.included$`10/18/16`$cells)]

ict.sig = signatureAll(et.nms_only[,c(ict_cells.keep,nict_cells.keep)],ict_cells.keep)
ictal_signature = ict.sig[ict.sig$log2FC>=2 & ict.sig$pval<=.05 & ict.sig$meanA>=50,]
nonictal_signature = ict.sig[ict.sig$log2FC<=1/2 & ict.sig$pval<=.05 & ict.sig$meanB>=50,]

write.csv(ictal_signature,file = "~/Desktop/usc/grants/CUREepilepsy-4.2017/rm10-11/up_ictal-10&11rm.csv")
write.csv(nonictal_signature,file = "~/Desktop/usc/grants/CUREepilepsy-4.2017/rm10-11/dn_ictal-10&11rm.csv")

up_ictal = capitalize(tolower(rownames(ictal_signature)))
dn_ictal = capitalize(tolower(rownames(nonictal_signature)))
toAffy(dn_ictal,up_ictal,outbase = "~/Desktop/usc/grants/CUREepilepsy-4.2017/rm10-11")
trans_sig_drug_priority = priorityCmap("~/Desktop/usc/grants/CUREepilepsy-4.2017/rm10-11/permutedResults151134.xls",weightMat = c(5,2,5),betaMat = c(1/4,3,6),reordered_parent = "/Users/maxwellbay/Desktop/usc/grants/CUREepilepsy-4.2017/rm10-11")


###
#11self 
ict_cells.keep = ict_cells[ict_cells %in% c(dates_dict.included$`11/1/16`$cells)]
nict_cells.keep = nict_cells[nict_cells %in% c(dates_dict.included$`11/1/16`$cells)]

ict.sig = signatureAll(et.nms_only[,c(ict_cells.keep,nict_cells.keep)],ict_cells.keep)
ictal_signature = ict.sig[ict.sig$log2FC>=2 & ict.sig$pval<=.05 & ict.sig$meanA>=50,]
nonictal_signature = ict.sig[ict.sig$log2FC<=1/2 & ict.sig$pval<=.05 & ict.sig$meanB>=50,]

write.csv(ictal_signature,file = "~/Desktop/usc/grants/CUREepilepsy-4.2017/11self/up_ictal-11self.csv")
write.csv(nonictal_signature,file = "~/Desktop/usc/grants/CUREepilepsy-4.2017/11self/dn_ictal-11self.csv")

up_ictal = capitalize(tolower(rownames(ictal_signature)))
dn_ictal = capitalize(tolower(rownames(nonictal_signature)))
toAffy(dn_ictal,up_ictal,outbase = "~/Desktop/usc/grants/CUREepilepsy-4.2017/11self/")
trans_sig_drug_priority = priorityCmap("~/Desktop/usc/grants/CUREepilepsy-4.2017/11self/permutedResults151161.xls",weightMat = c(5,2,5),betaMat = c(1/4,3,6),reordered_parent = "/Users/maxwellbay/Desktop/usc/grants/CUREepilepsy-4.2017/11self")


#12self
ict_cells.keep = ict_cells[ict_cells %in% c(dates_dict.included$`12/16/16`$cells)]
nict_cells.keep = nict_cells[nict_cells %in% c(dates_dict.included$`12/16/16`$cells)]

ict.sig = signatureAll(et.nms_only[,c(ict_cells.keep,nict_cells.keep)],ict_cells.keep)
ictal_signature = ict.sig[ict.sig$log2FC>=2 & ict.sig$pval<=.05 & ict.sig$meanA>=50,]
nonictal_signature = ict.sig[ict.sig$log2FC<=1/2 & ict.sig$pval<=.05 & ict.sig$meanB>=50,]

write.csv(ictal_signature,file = "~/Desktop/usc/grants/CUREepilepsy-4.2017/12self/up_ictal-12self.csv")
write.csv(nonictal_signature,file = "~/Desktop/usc/grants/CUREepilepsy-4.2017/12self/dn_ictal-12self.csv")

up_ictal = capitalize(tolower(rownames(ictal_signature)))
dn_ictal = capitalize(tolower(rownames(nonictal_signature)))
toAffy(dn_ictal,up_ictal,outbase = "~/Desktop/usc/grants/CUREepilepsy-4.2017/12self/")
trans_sig_drug_priority = priorityCmap("~/Desktop/usc/grants/CUREepilepsy-4.2017/12self/permutedResults151141.xls",weightMat = c(5,2,5),betaMat = c(1/4,3,6),reordered_parent = "/Users/maxwellbay/Desktop/usc/grants/CUREepilepsy-4.2017/12self")

###
#11&12pooled
ict_cells.keep = ict_cells[ict_cells %in% c(dates_dict.included$`11/1/16`$cells,dates_dict.included$`12/16/16`$cells)]
nict_cells.keep = nict_cells[nict_cells %in% c(dates_dict.included$`11/1/16`$cells,dates_dict.included$`12/16/16`$cells)]

ict.sig = signatureAll(et.nms_only[,c(ict_cells.keep,nict_cells.keep)],ict_cells.keep)
ictal_signature = ict.sig[ict.sig$log2FC>=2 & ict.sig$pval<=.05 & ict.sig$meanA>=50,]
nonictal_signature = ict.sig[ict.sig$log2FC<=1/2 & ict.sig$pval<=.05 & ict.sig$meanB>=50,]

write.csv(ictal_signature,file = "~/Desktop/usc/grants/CUREepilepsy-4.2017/11&12pooled/up_ictal-11&12pooled.csv")
write.csv(nonictal_signature,file = "~/Desktop/usc/grants/CUREepilepsy-4.2017/11&12pooled/dn_ictal-11&12pooled.csv")

up_ictal = capitalize(tolower(rownames(ictal_signature)))
dn_ictal = capitalize(tolower(rownames(nonictal_signature)))
toAffy(dn_ictal,up_ictal,outbase = "~/Desktop/usc/grants/CUREepilepsy-4.2017/11&12pooled/")
trans_sig_drug_priority = priorityCmap("~/Desktop/usc/grants/CUREepilepsy-4.2017/11&12pooled/permutedResults151160.xls",weightMat = c(5,2,5),betaMat = c(1/4,3,6),reordered_parent = "/Users/maxwellbay/Desktop/usc/grants/CUREepilepsy-4.2017/11&12pooled")


###
#9a&9b&5
ict_cells.keep = ict_cells[ict_cells %in% c(dates_dict.included$`9/8/15`$cells,dates_dict.included$`9/17/15`$cells,dates_dict.included$`5/5/16`$cells)]
nict_cells.keep = nict_cells[nict_cells %in% c(dates_dict.included$`9/8/15`$cells,dates_dict.included$`9/17/15`$cells,dates_dict.included$`5/5/16`$cells)]

ict.sig = signatureAll(et.nms_only[,c(ict_cells.keep,nict_cells.keep)],ict_cells.keep)
ictal_signature = ict.sig[ict.sig$log2FC>=2 & ict.sig$pval<=.05 & ict.sig$meanA>=50,]
nonictal_signature = ict.sig[ict.sig$log2FC<=1/2 & ict.sig$pval<=.05 & ict.sig$meanB>=50,]

write.csv(ictal_signature,file = "~/Desktop/usc/grants/CUREepilepsy-4.2017/9a&9b&5/up_ictal-9a&9b&5.csv")
write.csv(nonictal_signature,file = "~/Desktop/usc/grants/CUREepilepsy-4.2017/9a&9b&5/dn_ictal-9a&9b&5.csv")

up_ictal = capitalize(tolower(rownames(ictal_signature)))
dn_ictal = capitalize(tolower(rownames(nonictal_signature)))
toAffy(dn_ictal,up_ictal,outbase = "~/Desktop/usc/grants/CUREepilepsy-4.2017/9a&9b&5/")
trans_sig_drug_priority = priorityCmap("~/Desktop/usc/grants/CUREepilepsy-4.2017/9a&9b&5/permutedResults151201.xls",weightMat = c(5,2,5),betaMat = c(1/4,3,6),reordered_parent = "/Users/maxwellbay/Desktop/usc/grants/CUREepilepsy-4.2017/9a&9b&5")






################################# EXAMPLE CODE #####################################
prob_matrix=replicate(100, rnorm(20))
drug_names=paste("drug",letters[1:20],sep="_")
patient_ids=paste("patient",c(1:100),sep="_")
rownames(prob_matrix)=drug_names
colnames(prob_matrix)=patient_ids

#Create fake color side bars
drugclass_colors=sample(c("darkorchid","darkred"), length(drug_names), replace = TRUE, prob = NULL)
drugcategory_colors=sample(c("green","darkgreen"), length(drug_names), replace = TRUE, prob = NULL)
subtype_colors=sample(c("red","blue","cyan","pink","yellow","green"), length(patient_ids), replace = TRUE, prob = NULL)
Mcolors=sample(c("black","white","grey"), length(patient_ids), replace = TRUE, prob = NULL)
Ncolors=sample(c("black","white","grey"), length(patient_ids), replace = TRUE, prob = NULL)
Tcolors=sample(c("black","white","grey"), length(patient_ids), replace = TRUE, prob = NULL)
HER2colors=sample(c("black","white","grey"), length(patient_ids), replace = TRUE, prob = NULL)
PRcolors=sample(c("black","white","grey"), length(patient_ids), replace = TRUE, prob = NULL)
ERcolors=sample(c("black","white","grey"), length(patient_ids), replace = TRUE, prob = NULL)
rlab=t(cbind(drugclass_colors,drugcategory_colors))
clab=cbind(subtype_colors,Mcolors,Ncolors,Tcolors,HER2colors,PRcolors,ERcolors)
rownames(rlab)=c("Class","Category")
colnames(clab)=c("Subtype","M","N","T","HER2","PR","ER")

#Define custom dist and hclust functions for use with heatmaps
mydist=function(c) {dist(c,method="euclidian")}
myclust=function(c) {hclust(c,method="average")}

#Create heatmap using custom heatmap.3 source code loaded above
pdf(file="heatmap3_example.pdf")
main_title="Drug Response Predictions"
par(cex.main=1)
heatmap.3(prob_matrix, hclustfun=myclust, distfun=mydist, na.rm = TRUE, scale="none", dendrogram="both", margins=c(6,12),
          Rowv=TRUE, Colv=TRUE, ColSideColors=clab, RowSideColors=rlab, symbreaks=FALSE, key=TRUE, symkey=FALSE,
          density.info="none", trace="none", main=main_title, labCol=FALSE, labRow=drug_names, cexRow=1, col=rev(heat.colors(75)),
          ColSideColorsSize=7, RowSideColorsSize=2, KeyValueName="Prob. Response")
legend("bottomleft",legend=c("Basal","LumA","LumB","Her2","Claudin","Normal","","Positive","Negative","NA","","Targeted","Chemo","","Approved","Experimental"),
       fill=c("red","blue","cyan","pink","yellow","green","white","black","white","grey","white","darkorchid","darkred","white","green","darkgreen"), border=FALSE, bty="n", y.intersp = 0.7, cex=0.7)
dev.off()


>>>>>>> 09d2c253d0f5f15834553e91ba3f88d66d9773d6
