source("src/Waterfall.R")

library("org.Hs.eg.db")

loadShinGenes = function(new=FALSE){
  unit = "expected_count"
  
  shin_data_genes = file.path("data","shin_genes.csv")
  #generates expression matrix - genes
  if ((!file.exists(shin_data_genes))|new){
    files = list.files("data/shin_STAR/samples")
    for (i in 1:length(files)){
      file_name = files[i]
      samp_name = file_name
      path_to_file = file.path("data","shin_STAR","samples",file_name)
      
      tmp = as.data.frame(read.table(path_to_file,header = TRUE))
      
      expr = as.numeric(tmp[,unit])
      names(expr) = as.character(tmp[,"gene_id"])
      
      if (i==1){
        all.mat = as.data.frame(matrix(expr,nrow = nrow(tmp),ncol=1))
        rownames(all.mat) = names(expr)
        colnames(all.mat) = samp_name
        
        gen_lengths = tmp$effective_length
        names(gen_lengths) = names(expr)
      }else{
        all.mat[names(expr),samp_name] = expr
      }
    }
    
    cnms = c("gen_lengths",colnames(all.mat))
    all.mat = cbind(gen_lengths,all.mat)
    colnames(all.mat) = cnms
    write.csv(all.mat,shin_data_genes)
  }
  
  #load all data
  return(as.data.frame(as.matrix(read.table(shin_data_genes,header = TRUE,row.names = 1,sep = ","))))
  
}

#load data
jt.gns = loadShinGenes() #assign 'new = TRUE' to generate new expression table

#split ensembl ID and gene ID and replace rownames
rnms = rownames(jt.gns)
ensID = unlist(lapply(strsplit(rnms,split="_"),function(l){return(l[1])}))
genNM = capitalize(tolower(unlist(lapply(strsplit(rnms,split="_"),function(l){return(paste(l[2:length(l)],collapse="_"))}))))
names(genNM) = ensID
rownames(jt.gns) = ensID 

#protein coding subset#
#PROT_NAME = "UNIPROT" #"UNIGENE"
#all_genes = rownames(jt.gns)
#prot_asso = AnnotationDbi::select(org.Mm.eg.db,all_genes,PROT_NAME,"ENSEMBL")
#pro_genes = unique(prot_asso[!is.na(prot_asso[,PROT_NAME]),"ENSEMBL"])
#jt.gns = jt.gns[pro_genes,]

#pull asside CPM
jt.cts = ScaleTPM(jt.gns[,colnames(jt.gns)!="gen_lengths"])

#compute TPM
jt.gns = ScaleTPM(raw2FPKMLengthKnown(jt.gns,1))

#get cols from Shin et all alignment
jt.gns = jt.gns[,colnames(jt)]
jt.cts = jt.cts[,colnames(jt)]
jt.gns.cols = jt.cols

grx.gns = Waterfall.Cluster(jt.gns,cell.cols = jt.gns.cols,nClusters = 6)
jt.gns.cols = grx.gns[,1]

jt.gns.pt = Waterfall.Pseudotime(jt.gns,col=jt.gns.cols,mst = F,threeD_plot = TRUE)

jt.gns.cols = jt.gns.cols[rownames(jt.gns.pt)]
jt.gns = jt.gns[,rownames(jt.gns.pt)]
jt.gns["Pseudotime",] = as.numeric(jt.gns.pt[,1])

pt_cor = apply(jt.gns,1,function(v){return(cor(as.numeric(jt.gns["Pseudotime",]),as.numeric(v),method = "spearman"))})
pt_cor = pt_cor[!is.na(pt_cor)]
pt_cor_up = sort(pt_cor[pt_cor>0],decreasing = TRUE)
pt_cor_dn = sort(pt_cor[pt_cor<0])

gene_selection = genNM[names(pt_cor_up[6])]
PTgeneplot(ensID[grep(gene_selection,genNM)],as.data.frame(jt.gns),col=jt.gns.cols,hmm = TRUE,gene_txt = gene_selection)
PTgeneplot(gene_selection,jt,col=jt.cols,hmm = TRUE)

  
#
  




