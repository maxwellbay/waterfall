#!/bin/bash

ESEARCH=/auto/rcf-proj/mmb1/maxwellb/bin/edirect/esearch
EFETCH=/auto/rcf-proj/mmb1/maxwellb/bin/edirect/efetch
PREFETCH=/home/rcf-proj/mmb1/maxwellb/bin/sratoolkit.2.8.2-1-centos_linux64/bin/prefetch

if [ $# -eq 0 ];then
	echo "Missing GSE"
	echo "<basename> <GSE number>"
fi
GSE=$1
#GSE="GSE71485"

srsx=`$ESEARCH -db gds -query $GSE | $EFETCH -format docsum | xtract -pattern ExtRelation -element TargetObject | grep 'SRX'`

for srx in $srsx;do
	srr=`$ESEARCH -db sra -query $srx | $EFETCH -format docsum | xtract -pattern DocumentSummary -element Run@acc`
	cmd="$PREFETCH $srr"
	eval $cmd
done 


