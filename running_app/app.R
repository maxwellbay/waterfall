# Load packages ----
library(shiny)
library(scales)
library(ggplot2)
library(hexbin)
library(tidyverse)
library(ggrepel)
library(RColorBrewer)
library(dqshiny)
library(broom)
library(DT)

# Load data ----

rn.mat <- readRDS("data/rn.mat_1.rds")
umap_coords <- readRDS("data/umap_coords_1.rds")
lab_df <- readRDS("data/lab_df_1.rds")

#k_cols <- readRDS("data/k_cols_1.rds")
#rn.markers <- readRDS("data/rn_markers_1.rds")

genes <-  sort(rownames(rn.mat))
umap_coords$cell_type <- factor(umap_coords$cell_type, levels = as.character(unique(umap_coords$cell_type)))


cell_type_lst <- 1:length(levels(umap_coords$cell_type))
names(cell_type_lst) <- levels(umap_coords$cell_type)
cell_type_lst <- as.list(cell_type_lst)

# Source helper functions -----
source("helper.R")

# User interface ----
ui <- fluidPage(
  
  titlePanel("Running Gene Expression Visualization"),
  fluidRow(
    column(4,
           autocomplete_input("gene","Enter a gene to visualize expression",genes,max_options = 25,placeholder = "CFPcust (for example)"),
           radioButtons("split", label = "Split options",
                        choices = list("Merge conditions" = 1, "Split by condition" = 2))
           ),

    column(3,
           radioButtons("hex", label = "Cell options (UMAP ONLY)",
                        choices = list("Individual cells" = 1, "Hex bins" = 2)),
           sliderInput("p_size", label = "Cell/Hex size", min = 1, 
                       max = 100, value = 50,post = "%")
           ),
    column(3,
           radioButtons("color_by", label = "Color by (UMAP ONLY)",
                        choices = list("Genes" = 1, "Condition" = 2,"Cell Type" = 3))
    )

  ),
  hr(),
  
  tabsetPanel(
    tabPanel("UMAP",
             column(10,
                    plotOutput("UMAP",width = "1100px",height = "900px"),
                    align = "center")),
    tabPanel("Violin",
             column(10,
                    plotOutput("Violin",width = "1200px",height = "500px"),
                    align = "center")),
    tabPanel("Statistical Test",
             column(3,
                    radioButtons("cell_type_selection",label = "Select Cell Type",
                                 choices = cell_type_lst,width = "100%")
                    ),
             column(9,
                    h3("Wilcox rank sum test"),
                    DT::dataTableOutput("statistics") ))
  )
  

)

# Server logic ----
server <- function(input, output,session) {
  
  update_autocomplete_input(session,"gene",placeholder = "",options = genes)
  
  output$UMAP <- renderPlot({
    
    gene <- ifelse(input$gene=="","CFPcust",input$gene)
    split <- ifelse(input$split==1,FALSE,TRUE)
    hex <- ifelse(input$hex==1,FALSE,TRUE)
    p_size <- input$p_size
    color_by <- input$color_by
    
    UMAP_plot(coords_condition = umap_coords,
              exp_mat = rn.mat,
              gene_selection = gene,
              label_df = lab_df,
              split_by_condition = split,
              as_hex = hex,
              add_label = TRUE,
              p_size = p_size,
              color_by = color_by)
    
    
  })
  
  output$Violin <- renderPlot({
    gene <- ifelse(input$gene=="","CFPcust",input$gene)
    split <- ifelse(input$split==1,FALSE,TRUE)
    
    Violin_plot(umap_coords = umap_coords,
                exp_mat = rn.mat,
                gene_select = gene,
                split_by_condition = split)
    
  })
  
  output$statistics <- DT::renderDataTable({
    gene <- ifelse(input$gene=="","CFPcust",input$gene)
    
    selected_type <- names(cell_type_lst)[as.integer(input$cell_type_selection)]
    
    tmp_df <- umap_coords %>% 
      mutate(expression = as.numeric(rn.mat[gene,])) %>% 
      filter(cell_type == selected_type)
    
    gene_expression <- tmp_df$expression
    group <- tmp_df$condition
    
    test.out <- pairwise.wilcox.test(gene_expression,group,p.adj = "BH")
    tidy_test <- tidy(test.out)
    
    stat_df <- tidy_test %>% 
      mutate(group1_mean = sapply(group1,function(x){mean(gene_expression[group==x])}),
             group2_mean = sapply(group2,function(x){mean(gene_expression[group==x])}),
             log2FC = log2(as.numeric(group1_mean)/as.numeric(group2_mean)),
             group1_pctDetected = sapply(group1,function(x){
               expr = gene_expression[group ==x]
               return(100*sum(expr >0)/length(expr))
             }),
             group2_pctDetected = sapply(group2,function(x){
               expr = gene_expression[group ==x]
               return(100*sum(expr >0)/length(expr))
             })
             ) %>% 
      arrange(desc(log2FC)) %>% 
      mutate("Group 1" = group1,
             "Group 2" = group2,
             "P-Val" = format.pval(p.value,
                                   digits = 2),
             "G1 mean" = round(group1_mean,2),
             "G2 mean" = round(group2_mean,2),
             "Log2 FC" = round(log2FC,2),
             "G1 % pos" = round(group1_pctDetected,2),
             "G2 % pos" = round(group2_pctDetected,2)) %>% 
      select("Group 1","Group 2","P-Val","G1 mean","G2 mean","Log2 FC","G1 % pos","G2 % pos")
    
    return(stat_df)
    
  },
  options = list(pageLength = 15, lengthChange = FALSE, paging = FALSE,searching = FALSE))
  
}

# Run app ----
shinyApp(ui, server)