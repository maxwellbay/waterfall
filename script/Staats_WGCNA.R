source("src/network_analysis.R")
source("src/HyperGeoTerms.R")

library(RColorBrewer)
library(Seurat)
library(gridExtra)

legend.col <- function(col, lev){
  
  opar <- par
  
  n <- length(col)
  
  bx <- par("usr")
  
  box.cx <- c(bx[2] + (bx[2] - bx[1]) / 1000,
              bx[2] + (bx[2] - bx[1]) / 1000 + (bx[2] - bx[1]) / 50)
  box.cy <- c(bx[3], bx[3])
  box.sy <- (bx[4] - bx[3]) / n
  
  xx <- rep(box.cx, each = 2)
  
  par(xpd = TRUE)
  for(i in 1:n){
    
    yy <- c(box.cy[1] + (box.sy * (i - 1)),
            box.cy[1] + (box.sy * (i)),
            box.cy[1] + (box.sy * (i)),
            box.cy[1] + (box.sy * (i - 1)))
    polygon(xx, yy, col = col[i], border = col[i])
    
  }
  par(new = TRUE)
  plot(0, 0, type = "n",
       ylim = c(min(lev), max(lev)),
       yaxt = "n", ylab = "",
       xaxt = "n", xlab = "",
       frame.plot = FALSE)
  axis(side = 4, las = 2, tick = FALSE, line = .25)
  par <- opar
}


if (F){
  #import cts
  staats.mat = read.csv(file = "data/staats_data/countPerSample_4Max_ks.csv",header = TRUE,row.names = 1)
  
  #import sample annotation
  staats.cond = read.csv(file = "data/staats_data/conditions_all4.csv",header = TRUE)
  
  #fix false cnms
  colnames(staats.mat) = sub("X","",colnames(staats.mat))
  
  #from kim 
  staats.mat.log = log2(staats.mat+1)
  
  staats.grx = Waterfall.Cluster(staats.mat.log) # rld
  staats.col = staats.grx[,1]
  
  staats.pca =  Waterfall.PCA(staats.mat.log,col = staats.col, plot_title = "PCA - K. Staats")
  
  
  top_var = apply(staats.mat.log,1,sd)
  top_var = sort(top_var,decreasing = TRUE)
  top_var = names(top_var[1:5000])
  
  staats.wgcna = networkAnalysisBlock(t(staats.mat.log[top_var,]),minModuleSize = 100,
                                      softPower = 9,block_save_dir = "~/Desktop/staats_dat")
  
  
  MEList = moduleEigengenes(staats.wgcna$datMat, colors = staats.wgcna$modColors)
  
  staats.eig = t(MEList$eigengenes)
  colnames(staats.eig) = colnames(staats.mat)
  
  staats.eig = apply(staats.eig,1,function(v){
    return(v - min(v))
  })
  
  staats.eig = t(staats.eig)
  
  pheatmap(staats.eig)
  
  als_cls = grep("c1",colnames(staats.mat),value = TRUE,invert = TRUE)
  
  staats.wrs = signatureAll(staats.eig,als_cls)
  
  #staats.wgcna$moduleMat[,which("yellow",)]
  
  hub = plotHubGenes(staats.wgcna$moduleMat,"yellow",top=10,lgnd=FALSE)
  
  cor_mat = cor(t(staats.mat.log))
  #CHCHD2 correlations
  chchd2_cor = sort(cor_mat["ENSG00000106153",],decreasing = TRUE)
  chchd2_acor = sort(cor_mat["ENSG00000106153",])
  
  
  
  
  
}

#####
#single cell stuff 
non_neuronal =  as.character(read.table("data/staats_data/non_neuronal_cells.txt",header = FALSE)[,1])

pbmc.data = Read10X("data/staats_data/_from_Kim/outs/filtered_gene_bc_matrices_mex/GRCh38/")

pbmc = CreateSeuratObject(raw.data = pbmc.data, min.cells = 3, min.genes = 200, 
                           project = "staats_CRISPRcor_IPCMN")

pbmc = SubsetData(pbmc, cells.use = colnames(pbmc@raw.data)[!colnames(pbmc@raw.data)%in%non_neuronal])

sts.pat = sts.grp = c("p1","p2","p1","p2")[as.numeric(unlist(lapply(strsplit(pbmc@cell.names,"-"),function(l)return(l[2]))))]
sts.trt = sts.grp = c("control","control","c9","c9")[as.numeric(unlist(lapply(strsplit(pbmc@cell.names,"-"),function(l)return(l[2]))))]
sts.grp = paste0(sts.pat,"_",sts.trt)

names(sts.pat) = pbmc@cell.names
names(sts.trt) = pbmc@cell.names
names(sts.grp) = pbmc@cell.names

mito.genes = grep(pattern = "^MT-", x = rownames(x = sts.all$pmbc@data), value = TRUE)
percent.mito = Matrix::colSums(pbmc@raw.data[mito.genes, ])/Matrix::colSums(pbmc@raw.data)

# AddMetaData adds columns to object@meta.data, and is a great place to
# stash QC stats
pbmc = AddMetaData(object = pbmc, metadata = percent.mito, col.name = "percent.mito")
pbmc = AddMetaData(object = pbmc, metadata = sts.pat, col.name = "patient")
pbmc = AddMetaData(object = pbmc, metadata = sts.trt, col.name = "treatment")
pbmc@ident = factor(sts.trt,levels = unique(sts.trt))

VlnPlot(object = pbmc, features.plot = c("nGene", "nUMI", "percent.mito"), nCol = 3)

GenePlot(object = pbmc, gene1 = "nUMI", gene2 = "percent.mito")
GenePlot(object = pbmc, gene1 = "nUMI", gene2 = "nGene")

pbmc = NormalizeData(object = pbmc, normalization.method = "LogNormalize", scale.factor = 10000,display.progress = TRUE)


pbmc = FindVariableGenes(object = pbmc, mean.function = ExpMean, dispersion.function = LogVMR, 
                          x.low.cutoff = 0.0125, x.high.cutoff = 3, y.cutoff = 0.5)

pbmc <- ScaleData(object = pbmc,vars.to.regress = c("nUMI","patient") ,do.par = TRUE, num.cores = 7,
                  do.scale = FALSE,do.center = FALSE)

pbmc@scale.data = as.matrix(pbmc@data)

pbmc <- RunPCA(object = pbmc, pc.genes = pbmc@var.genes, do.print = TRUE, pcs.print = 1:5, 
               genes.print = 5)

VizPCA(object = pbmc, pcs.use = 1:2)

PCAPlot(object = pbmc, dim.1 = 1, dim.2 = 2)

PCHeatmap(object = pbmc, pc.use = 1, cells.use = 500, do.balanced = TRUE, label.columns = FALSE)


pbmc <- RunTSNE(object = pbmc, dims.use = 1:5, do.fast = TRUE,perplexity = 20)

TSNEPlot(object = pbmc)



sts.scd = as.matrix(pbmc@scale.data)

sts.tsne = pbmc@dr$tsne@cell.embeddings
sts.pca = pbmc@dr$pca@cell.embeddings

#1 = c1    = patient1 control
#2 = c18   = patient2 control
#3 = 6769  = patient1 c9
#4 = 10689 = patient2 c9

adx = c("p1_control" = "#EB537C",
        "p2_control" = "#FFA55A",
        "p1_c9" = "#96354F",
        "p2_c9" = "#A56B3A")



sts.cols = adx[sts.grp]

var = apply(pbmc@raw.data,1,sd)
mean = apply(pbmc@raw.data,1,mean)
cv = var/mean

top_var = names(sort(var,decreasing = TRUE)[1:5000])

sts.wgcna = networkAnalysisBlock(t(sts.scd[top_var,]),softPower = 3,minModuleSize = 10)

sts.eigen = moduleEigengenes(sts.wgcna$datMat,sts.wgcna$modColors)
sts.eg = t(sts.eigen$eigengenes); colnames(sts.eg) = rownames(sts.wgcna$datMat) 

sts.all = list(pbmc = pbmc,
               scd = sts.scd,
               tsne = sts.tsne,
               pca = sts.pca,
               cols = sts.cols,
               wgcna = sts.wgcna,
               eigen = sts.eigen,
               eg = sts.eg)

saveRDS(sts.all,file = "~/Desktop/staats/sts.all.rds")

# READY READY READY #
# READY READY READY #
# READY READY READY #
# READY READY READY #
# READY READY READY #

sts.all = readRDS("~/Desktop/staats/sts.all.rds")

adx = c("p1_control" = "#EB537C",
        "p2_control" = "#FFA55A",
        "p1_c9" = "#96354F",
        "p2_c9" = "#A56B3A")


#check for chchd2 exp correlation
d2_exp = sts.all$scd["CHCHD2",]  
cor_vect = apply(sts.all$scd,1,function(v){return(cor(v,as.numeric(d2_exp)))})
cor_vect2 = sort(cor_vect,decreasing = TRUE)

write.csv(cor_vect2,file = "~/Desktop/staats/chchd2_correlations.csv")

pdf(file = "~/Desktop/staats/staats_genes_tsne_eigen.pdf",width = 6,height = 9)
par(mar = c(3,3,3,3),mfrow = c(3,2))
cex = .2
plot(sts.all$tsne,col = sts.all$cols,cex = cex,pch = 19,main = "Combined | t-SNE", xlab = "tSNE 1", ylab = "tSNE 2")
legend("bottomleft",legend  = names(adx)[c(1,3,2,4)], fill = adx[c(1,3,2,4)],bty = "n", cex = .7)

lngth = 100
marker = colorRampPalette(brewer.pal(11,"Spectral"))(lngth)[lngth:1]

for (i in 1:nrow(sts.all$eg)){
  eg = rownames(sts.all$eg)[i]

  exp = sts.all$eg[i,]
  exp_norm = exp - min(exp)
  exp_norm = round((exp_norm/max(exp_norm))*(lngth-1),0)+1
  
  clrs = marker[exp_norm]
  plot(sts.all$tsne[,1:2],cex = cex,xlab = "tSNE 1",ylab = "tSNE 2",main = eg)
  points(sts.all$tsne[,1:2],cex = cex-(.1*cex),col = alpha(clrs, 1),pch = 19)
  
  legend.col(col = marker, lev = as.numeric(exp))
  
}
dev.off()

query_genes = c("CHCHD2", "MNX1", "CHAT", "TUBB3","MAP2")
pdf(file = "~/Desktop/staats/staats_genes_tsne_goe.pdf",width = 10,height = 6.66)
par(mar = c(3,3,3,3),mfrow = c(2,3))
cex = .2
plot(sts.all$tsne,col = sts.all$cols,cex = cex,pch = 19,main = "Combined | t-SNE", xlab = "tSNE 1", ylab = "tSNE 2")
legend("bottomleft",legend  = names(adx)[c(1,3,2,4)], fill = adx[c(1,3,2,4)],bty = "n", cex = .7)

lngth = 100
marker = colorRampPalette(brewer.pal(11,"Spectral"))(lngth)[lngth:1]
for (gene in query_genes){
  
  exp = sts.all$scd[gene,]
  exp_norm = exp - min(exp)
  exp_norm = round((exp_norm/max(exp_norm))*(lngth-1),0)+1

  clrs = marker[exp_norm]
  plot(sts.all$tsne[,1:2],cex = cex,xlab = "tSNE 1",ylab = "tSNE 2",main = gene)
  points(sts.all$tsne[,1:2],cex = cex-(.1*cex),col = clrs,pch = 19)
  
  legend.col(col = marker, lev = as.numeric(exp))
  
}
dev.off()

#
genes = c("RPS25", "MRPS25")

pdf(file = "~/Desktop/staats/staats_genes_tsne_RPSstuff.pdf",width = 9,height = 3)
par(mar = c(3,3,3,3),mfrow = c(1,3))
cex = .2
plot(sts.all$tsne,col = sts.all$cols,cex = cex,pch = 19,main = "Combined | t-SNE", xlab = "tSNE 1", ylab = "tSNE 2")
legend("bottomleft",legend  = names(adx)[c(1,3,2,4)], fill = adx[c(1,3,2,4)],bty = "n", cex = .7)

lngth = 100
marker = colorRampPalette(brewer.pal(11,"Spectral"))(lngth)[lngth:1]
for (i in 1:2){
  
  gene = genes[i]
  cor_val = cor_vect2[i]
  
  exp = sts.all$scd[gene,]
  exp_norm = exp - min(exp)
  exp_norm = round((exp_norm/max(exp_norm))*(lngth-1),0)+1
  
  clrs = marker[exp_norm]
  plot(sts.all$tsne[,1:2],cex = cex,xlab = "tSNE 1",ylab = "tSNE 2",main = gene)
  points(sts.all$tsne[,1:2],cex = cex-(.1*cex),col = clrs,pch = 19)
  
  legend.col(col = marker, lev = as.numeric(exp))
  
}
dev.off()


#check for xist exp correlation
#check for chchd2 exp correlation
d2_exp = sts.all$scd["CHCHD2",]  
cor_vect = apply(sts.all$scd,1,function(v){return(cor(v,as.numeric(d2_exp)))})
cor_vect2 = sort(cor_vect,decreasing = TRUE)

pdf(file = "~/Desktop/staats/staats_genes_tsne_chchd2_poscorr.pdf",width = 10,height = 6.66)
par(mar = c(3,3,3,3),mfrow = c(2,3))
cex = .2
plot(sts.all$tsne,col = sts.all$cols,cex = cex,pch = 19,main = "Combined | t-SNE", xlab = "tSNE 1", ylab = "tSNE 2")
legend("bottomleft",legend  = names(adx)[c(1,3,2,4)], fill = adx[c(1,3,2,4)],bty = "n", cex = .7)

lngth = 100
marker = colorRampPalette(brewer.pal(11,"Spectral"))(lngth)[lngth:1]
for (i in 1:30){
  
  gene = names(cor_vect2[i])
  cor_val = cor_vect2[i]
  
  exp = sts.all$scd[gene,]
  exp_norm = exp - min(exp)
  exp_norm = round((exp_norm/max(exp_norm))*(lngth-1),0)+1
  
  clrs = marker[exp_norm]
  plot(sts.all$tsne[,1:2],cex = cex,xlab = "tSNE 1",ylab = "tSNE 2",main = paste0(gene," r=",round(cor_val,2)))
  points(sts.all$tsne[,1:2],cex = cex-(.1*cex),col = clrs,pch = 19)
  
  legend.col(col = marker, lev = as.numeric(exp))
  
}
dev.off()

pdf(file = "~/Desktop/staats/staats_genes_tsne_chchd2_negcorr.pdf",width = 10,height = 6.66)
par(mar = c(3,3,3,3),mfrow = c(2,3))
cex = .2
plot(sts.all$tsne,col = sts.all$cols,cex = cex,pch = 19,main = "Combined | t-SNE", xlab = "tSNE 1", ylab = "tSNE 2")
legend("bottomleft",legend  = names(adx)[c(1,3,2,4)], fill = adx[c(1,3,2,4)],bty = "n", cex = .7)

lngth = 100
marker = colorRampPalette(brewer.pal(11,"Spectral"))(lngth)[lngth:1]
for (i in length(cor_vect2):(length(cor_vect2)-30)){
  
  gene = names(cor_vect2[i])
  cor_val = cor_vect2[i]
  
  exp = sts.all$scd[gene,]
  exp_norm = exp - min(exp)
  exp_norm = round((exp_norm/max(exp_norm))*(lngth-1),0)+1
  
  clrs = marker[exp_norm]
  plot(sts.all$tsne[,1:2],cex = cex,xlab = "tSNE 1",ylab = "tSNE 2",main = paste0(gene," r=",round(cor_val,2)))
  points(sts.all$tsne[,1:2],cex = cex-(.1*cex),col = clrs,pch = 19)
  
  legend.col(col = marker, lev = as.numeric(exp))
  
}
dev.off()



#go through x and y list
xym_table = read.xlsx2(file = "/Users/maxwellbay/Projects/Waterfall/data/staats_data/ensembl_hg38_chrMTXY.xlsx",sheetIndex = 1)
xy_gns = sort(unique(grep("MT-",xym_table$Gene.name,value = TRUE,invert = TRUE)))
xy_gns = xy_gns[xy_gns %in% rownames(sts.all$scd)]
xy_gns = names(sort(apply(sts.all$scd[rownames(sts.all$scd)%in%xy_gns,],1,mean),decreasing = TRUE))
pdf(file = "~/Desktop/staats/staats_genes_tsne_x_y_genes.pdf",width = 10,height = 6.66)
par(mar = c(3,3,3,3),mfrow = c(2,3))
cex = .2
plot(sts.all$tsne,col = sts.all$cols,cex = cex,pch = 19,main = "Combined | t-SNE", xlab = "tSNE 1", ylab = "tSNE 2")
legend("bottomleft",legend  = names(adx)[c(1,3,2,4)], fill = adx[c(1,3,2,4)],bty = "n", cex = .7)

lngth = 100
marker = colorRampPalette(brewer.pal(11,"Spectral"))(lngth)[lngth:1]
for (gene in xy_gns[1:30]){
  
  exp = sts.all$scd[gene,]
  exp_norm = exp - min(exp)
  exp_norm = round((exp_norm/max(exp_norm))*(lngth-1),0)+1
  
  clrs = marker[exp_norm]
  plot(sts.all$tsne[,1:2],cex = cex,xlab = "tSNE 1",ylab = "tSNE 2",main = gene)
  points(sts.all$tsne[,1:2],cex = cex-(.1*cex),col = clrs,pch = 19)
  
  legend.col(col = marker, lev = as.numeric(exp))


}
dev.off()




c9_genes = FindMarkers(sts.all$pbmc, ident.1 = "c9", ident.2 = "control")
c9_up = rownames(c9_genes)[c9_genes$p_val_adj<.05 & c9_genes$avg_logFC>0]
c9_dn = rownames(c9_genes)[which(c9_genes$p_val_adj<.05 & c9_genes$avg_logFC<0)]; c9_dn = c9_dn[length(c9_dn):1]

pdf(file = "~/Desktop/staats/staats_genes_tsne_degns.pdf",width = 9,height = 3)
par(mar = c(3,3,3,3),mfrow = c(1,3))
cex = .2
plot(sts.all$tsne,col = sts.all$cols,cex = cex,pch = 19,main = "Combined | t-SNE", xlab = "tSNE 1", ylab = "tSNE 2")
legend("bottomleft",legend  = names(adx)[c(1,3,2,4)], fill = adx[c(1,3,2,4)],bty = "n", cex = .7)

lngth = 100
marker = colorRampPalette(brewer.pal(11,"Spectral"))(lngth)[lngth:1]
for (dir in c("up","dn")){
  
  genes = get(paste0("c9_",dir))
  exp = apply(sts.all$scd[genes,],2,mean)
  exp_norm = exp - min(exp)
  exp_norm = round((exp_norm/max(exp_norm))*(lngth-1),0)+1
  
  clrs = marker[exp_norm]
  plot(sts.all$tsne[,1:2],cex = cex,xlab = "tSNE 1",ylab = "tSNE 2",main = dir)
  points(sts.all$tsne[,1:2],cex = cex-(.1*cex),col = alpha(clrs, 1),pch = 19)
  
  legend.col(col = marker, lev = as.numeric(exp))
  
}
dev.off()


sts.trt = sts.grp = c("control","control","c9","c9")[as.numeric(unlist(lapply(strsplit(sts.all$pbmc@cell.names,"-"),function(l)return(l[2]))))]
sts.pat = sts.grp = c("p1","p2","p1","p2")[as.numeric(unlist(lapply(strsplit(sts.all$pbmc@cell.names,"-"),function(l)return(l[2]))))]
sts.grp = paste0(sts.pat,"_",sts.trt)

lngth = 100
myPalette = colorRampPalette(rev(brewer.pal(11, "Spectral")))
sc = scale_colour_gradientn(colours = myPalette(100), limits=c(min(exp), max(exp)))

plot_lst = list()
for (gene in c9_up){
  exp = sts.all$scd[gene,]
  
  tsne_plot = data.frame(component1 = sts.all$tsne[,1], component2 = sts.all$tsne[,2], expr = exp)
  t = ggplot(tsne_plot, aes(x=component1, y=component2, colour=expr)) + 
    ggtitle(gene)+
    geom_point(size=1) + sc
  
  vexp = data.frame(expr = exp, grp = sts.trt)
  vexp$grp = factor(sts.trt,levels = c("control","c9"))
  vexp$pat = factor(sts.pat,levels = c("p1","p2"))
  
  bin = (max(vexp$expr) - min(vexp$expr))/100
  v = ggplot(vexp, aes(x=pat, y=expr, fill= grp)) + 
    ggtitle(gene)+
    geom_violin(trim = FALSE,alpha = .5)+
    geom_dotplot(binaxis='y', stackdir='center', dotsize = .1,binwidth = bin,
                 position=position_dodge()) + theme_bw()+
    ylim(0,max(vexp$expr)*1.15)+
    theme(plot.title = element_text(hjust = .5))

  
  #b = ggarrange(t,v,
  #              labels = c(paste(gene,"tSNE"),paste(gene,"Violin Plot")),
  #              ncol = 2, nrow = 1)
  
  #plot_lst[[gene]] =  b
  
  plot_lst[[paste0(gene,"_tSNE")]] =  t
  plot_lst[[paste0(gene,"_vplt")]] =  v
  
}
plots = marrangeGrob(grobs = plot_lst,nrow = 2,ncol = 4)

ggsave("~/Desktop/staats/staats_c9UP.pdf", plots,height = 9,width = 18,units = "in",limitsize = FALSE)



plot_lst = list()
for (gene in c9_dn){
  exp = sts.all$scd[gene,]
  
  tsne_plot = data.frame(component1 = sts.all$tsne[,1], component2 = sts.all$tsne[,2], expr = exp)
  t = ggplot(tsne_plot, aes(x=component1, y=component2, colour=expr)) + 
    ggtitle(gene)+
    geom_point(size=1) + sc
  
  vexp = data.frame(expr = exp, grp = sts.trt)
  vexp$grp = factor(sts.trt,levels = c("control","c9"))
  vexp$pat = factor(sts.pat,levels = c("p1","p2"))
  
  bin = (max(vexp$expr) - min(vexp$expr))/100
  v = ggplot(vexp, aes(x=pat, y=expr, fill= grp)) + 
    ggtitle(gene)+
    geom_violin(trim = FALSE,alpha = .5)+
    geom_dotplot(binaxis='y', stackdir='center', dotsize = .1,binwidth = bin,
                 position=position_dodge()) + theme_bw()+
    ylim(0,max(vexp$expr)*1.15)+
    theme(plot.title = element_text(hjust = .5))
  
  
  #b = ggarrange(t,v,
  #              labels = c(paste(gene,"tSNE"),paste(gene,"Violin Plot")),
  #              ncol = 2, nrow = 1)
  
  #plot_lst[[gene]] =  b
  
  plot_lst[[paste0(gene,"_tSNE")]] =  t
  plot_lst[[paste0(gene,"_vplt")]] =  v
  
}
plots = marrangeGrob(grobs = plot_lst,nrow = 2,ncol = 4)

ggsave("~/Desktop/staats/staats_c9DN.pdf", plots,height = 9,width = 18,units = "in",limitsize = FALSE)


