library(xlsx)

#import 
PROPORTION_DOC = "/Users/maxwellbay/Desktop/usc/my_publications/aging/sc_pop_prop/prop_combined.xlsx"
pop.dat = read.xlsx(PROPORTION_DOC,sheetName = "population",header = TRUE)
nes.dat = read.xlsx(PROPORTION_DOC,sheetName = "nestin",header = TRUE)
asc.dat = read.xlsx(PROPORTION_DOC,sheetName = "ascl1",header = TRUE)

#trim 24 mo from pop data
#pop.dat = pop.dat[-which(pop.dat$time==24),]

#convert time to days 
pop.dat$time = pop.dat$time * 30



#power law 
m = nls(rgl_prop ~ I(time^exp), data = pop.dat, start = list(exp = 1), 
        trace = TRUE)
smry = summary(m)

plot(pop.dat$rgl_prop ~ pop.dat$time,xlim = c(0,max(pop.dat[,1])),ylim = c(0,max(pop.dat$rgl_prop)))
s = seq(0,max(pop.dat$time),length = 1000)
lines(s, s^smry$coefficients[1],lty = 1, col = "blue")
lines(s, s^(-0.3098671),lty = 2, col = "blue")



#exponential decay - find base  
m = nls(rgl_prop ~ I(exp^time), data = pop.dat, start = list(exp = 1),
        #control = nls.control(maxiter = 1000,tol = 1e-150),
        trace = TRUE)
smry = summary(m)

plot(pop.dat$rgl_prop ~ pop.dat$time,xlim = c(0,max(pop.dat[,1])),ylim = c(0,max(pop.dat$rgl_prop)))
s = seq(0,max(pop.dat$time),length = 100)
lines(s, smry$coefficients[1]^s,lty = 1, col = "blue")
lines(s, (.996)^s,lty = 2, col = "blue")


#exponential decay - find tau 
m = nls(rgl_prop ~ I( max(pop.dat$rgl_prop) - (max(pop.dat$rgl_prop) - min(pop.dat$rgl_prop))*exp(1)^(-1*(time/tau)) ),
        data = pop.dat, start = list(tau = 1),trace = TRUE)
smry = summary(m)

plot(pop.dat$rgl_prop ~ pop.dat$time,xlim = c(0,max(pop.dat[,1])),ylim = c(0,max(pop.dat$rgl_prop)))
s = seq(0,max(pop.dat$time),length = 100)
#lines(s, predict(m,list(time = s)),lty = 1, col = "blue")
power = round(summary(m)$coefficients[1],3)
power.se = round(summary(m)$coefficients[2],3)
lines(s, 
      max(pop.dat$rgl_prop) - (max(pop.dat$rgl_prop) - min(pop.dat$rgl_prop))*exp(1)^(-1*(s/smry$coefficients[1]) ), 
      lty = 1, col = "blue")


#exponential decay - f9ind tau, A & D


A = min(pop.dat$rgl_prop)
D = abs(max(pop.dat$rgl_prop) - min(pop.dat$rgl_prop) )
tau = 5#smry$coefficients[1]
time = s

formula = A + (D*(exp(1)^-(time/tau)))


m = nls(rgl_prop ~ (A + (D*(exp(1)^-(time/tau))) ),
        data = pop.dat, start = list(tau = 5, A = .3, D = .5),trace = TRUE)

smry = summary(m)

plot(pop.dat$rgl_prop ~ pop.dat$time,xlim = c(0,max(pop.dat[,1])),ylim = c(0,max(pop.dat$rgl_prop)))
s = seq(0,max(pop.dat$time),length = 100)
#lines(s, predict(m,list(time = s)),lty = 1, col = "blue")
power = round(summary(m)$coefficients[1],3)
power.se = round(summary(m)$coefficients[2],3)
lines(s, 
      max(pop.dat$rgl_prop) - (max(pop.dat$rgl_prop) - min(pop.dat$rgl_prop))*exp(1)^(-1*(s/smry$coefficients[1]) ), 
      lty = 1, col = "blue")



p = .8
sample(c(TRUE,FALSE), size = 100, replace = TRUE, prob = c(p,1-p))