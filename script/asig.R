# 7/24/17:  The purpose of this file is to identify signature genes which are 
#           differentially upregulated in astrocytes in comparison to neural stem cells
#           in an attempt to identify those signature genes with an S cluster by
#           gene module constrained expression correlation (same method as was used
#           to identify a quiescent S module in qsig.R).
#
# STRUCTURE: Load multiple data sets (e.g. Bobadilla 2015, Zeisel 2015) and find consensus
#             genes, and look at consensus gene correlation to S cluster at bottom of script.


#####========= COMMON =========#####


## EXCLUDE QSIG CONSENSUS 
#source("scripts/qsig2.R")
#qsig_consensus = consensus_genes 

## settings 
bulk = FALSE 
load_rdata = TRUE
setup = FALSE

if (load_rdata) {
  load("asig.RData")
}

## parameters
fc_thresh = 2
log2fc_thresh = log2(fc_thresh)
p_thresh = 0.05
expr_thresh = 20
ftop = TRUE
weighted = TRUE
strict_thresh = FALSE
strict_consensus = TRUE
mean_thresh = TRUE # leave as TRUE until can filter both by median
filter_by_FC = FALSE
pairwise = FALSE

## TO DO
# data set bools for selection
# bool for iterative 
# why not source?
# boolean graph title construction? 

if (!(load_rdata) && setup) {
  # load priors
  source("load/LoadData.R")
  og_jt = jt
  library("openxlsx")
  library("WGCNA")
  library("wCorr")
  
  # Shin et al cluster objects (only q) and names
  Sclusters = list(S1.id, S2.id, S3.id, SAn.id)
  Snames = c("S1", "S2", "S3", "SA")
  jt = jt[, unlist(Sclusters)]
  
  
  # filter by top var
  if (ftop) {
    # determine top_var 
    gene_count = 5000
    top_var = apply(jt, 1, var)
    top_var = top_var[order(top_var, decreasing = TRUE)]
    top_var = names(top_var[1:gene_count])
    jt = jt[top_var,]
    archive_jt = jt
  }
  
  # create data expression (required for WGCNA_setup.R util)
  if (bulk) {
    # create bulk data expression 
    Sexprv <- function(s) {
      return(apply(jt[,s], 1, median))
    }
    Sexpr_df = sapply(Sclusters, Sexprv)
    colnames(Sexpr_df) = Snames
    datExpr = t(Sexpr_df)
  } else {
    # data expression is Shin et al data
    datExpr = t(jt)
  }
  
  # cluster data expression (produces "net" object)
  source("utils/WGCNA_setup.R")
  
  # load data sources
  ME_nums = sort(unique(net$colors))
  source("load/LoadData_Astro.R")
  
  # S cluster related objects
  Sclusters = list(S1.id, S2.id, S3.id, SAn.id)
  Snames = c("S1", "S2", "S3", "SA")
  
  # save RData (dont save automatically, must do manually to avoid unwanted overwrite)
  save.image("asig.RData")
}

#else {
#  if (setup) {
#    # load RData of prior run (has set FC thresh, etc.)
#    load("asig.RData")
#  }
#  #Sclusters = list(S1.id, S2.id, S3.id, SAn.id)
#  #Snames = c("S1", "S2", "S3", "SA")
#}

# calc GM expr by S

avgGM_byS <- function(s, g) {
  gene_in_jt = gene_by_color(g)[gene_by_color(g) %in% rownames(jt)]
  return(mean(apply(jt[gene_in_jt, s], 1, median)))
}
GM_allS <- function(g) {
  return(sapply(Sclusters, avgGM_byS, g))
}
GM_df = sapply(ME_nums, GM_allS)

# heatmap of GM expr by S
#par(cex.main = 0.6)
#heatmap(GM_df, Rowv = NA, scale = "column", col = brewer.pal(3, "Purples"), labRow = Snames, labCol = ME_nums,
#        xlab = "Module Eigengene", ylab = "S Cluster", main = "Module Eigengene Mean Expression by S Cluster")
# me_meanexpr_byS_heatmap.png


# GM expression vector by S
S_GMexprv <- function(s) {
  return(sapply(ME_nums, function(x){avgGM_byS(s, x)}))
}


# calc ME value by S
avgME_byS <- function(s, m) {
  # m is ME index number
  if (is.numeric(m)) {
    ME_name = paste("ME", as.character(m), sep = "")
  } else { ME_name = m }
  S_ind = as.numeric(which(colnames(jt) %in% s))
  return(median(net$MEs[S_ind, ME_name]))
}

ME_allS <- function(m) {
  return(sapply(Sclusters, avgME_byS, m))
}

ME_df = sapply(ME_nums, ME_allS)

# heatmap of ME value by S
#heatmap(ME_df, Rowv = NA, Colv = NA, scale = "column")

##### ================================ #####


# ~

#####======= BOBADILLA 2015 (data: "boba_df", "bdf")  =======#####
### compare both asigs to average S1-A, or compare both to neurons


if (!(pairwise)) {
  # find upregulated in astros 
  #fbdf = bdf[,colnames(bdf)[(grepl("PSA", colnames(bdf)) | grepl("GP", colnames(bdf))) | colnames(bdf) %in% boba_astro_ids]]
  fbdf = bdf
  astro_sig = signatureAll(fbdf, boba_astro_ids)
  ## exclude taps
  
  
  '
  # find upregulated in astros relative to S1-A (average)
  new_bdf = bdf[intersect(rownames(bdf), rownames(jt)), boba_astro_ids]
  int_bdf_jt_genes = intersect(rownames(jt), rownames(new_bdf))
  new_bdf = cbind(new_bdf[int_bdf_jt_genes, ], jt[int_bdf_jt_genes, ])
  astro_sig = signatureAll(new_bdf, boba_astro_ids)
  '
  
  
  # filter by mean expression
  if (strict_thresh) {
    asig = astro_sig[(astro_sig[,"meanA"] > expr_thresh & astro_sig[,"meanB"] > expr_thresh),]
  } else {
    asig = astro_sig[(astro_sig[,"meanA"] >= expr_thresh | astro_sig[,"meanB"] >= expr_thresh),]
  }
  
  
  # filter by p value and FC
  if (filter_by_FC) {
    asig = asig[((asig[,"pval"] < p_thresh) & (asig[,"log2FC"] >= log2fc_thresh)), ]
  } else {
    asig = asig[(asig[,"pval"] < p_thresh), ]
  }
  
  # list astro upgenes 
  boba_genes = rownames(asig)
  
  '
  # filter by expression
  if (mean_thresh) {
    boba_astro_expr = rowMeans(bdf[boba_genes, boba_astro_ids])
    boba_neuro_expr = rowMeans(bdf[boba_genes, boba_neuro_ids])
  } else {
    boba_astro_expr = apply(bdf[boba_genes, boba_astro_ids], 1, median)
    boba_neuro_expr = apply(bdf[boba_genes, boba_neuro_ids], 1, median)
  }
  boba_astro_inds = which(boba_astro_expr > expr_thresh)
  boba_neuro_inds = which(boba_neuro_expr > expr_thresh)
  if (strict_thresh) {
    boba_all_inds = boba_astro_inds
    #boba_all_inds = intersect(boba_astro_inds, boba_neuro_inds)
  } else {
    boba_all_inds = boba_astro_inds
    #boba_all_inds = union(boba_astro_inds, boba_neuro_inds)
  }
  '
  #boba_genes = boba_genes[boba_astro_inds]
  
} else {
  if (pairwise) {
    combined_genes = intersect(rownames(fbdf), rownames(jt))
    combined_df = cbind(jt[combined_genes,], fbdf[combined_genes,])
    ssigs = sapply(Sclusters, function(x){signatureAll(combined_df[,c(boba_astro_ids, x)], boba_astro_ids)})
    nssigs = apply(ssigs, 2, function(x){x[[1]][which((x[[2]] < p_thresh) & ((x[[6]] >= expr_thresh) | (x[[7]] >= expr_thresh)))]})
    nssigs = Reduce(intersect, nssigs)
    boba_genes = nssigs
  }
}




#####============----============#####

consensus_genes = boba_genes

# ~
#####======= BARRES 2014 (data: "barres_df")  =======#####
# filter by expression
#barres_genes = rownames(barres_df)[apply(barres_df, 1, function(x){max(x[c(1, 2)]) >= expr_thresh})]
# filter by FC (no p value available)
#barres_genes = barres_genes[barres_df[barres_genes, "FC"] > fc_thresh]
# filter by top_var
#barres_genes = barres_genes[which(barres_genes %in% top_var)]


## COMPARE TO SHIN ET AL DATA

top_in_barres = top_var[which(top_var %in% rownames(barres_df))]
# expression vectors
bse = barres_df[top_in_barres, "Astrocytes"]
jse = apply(jt[top_in_barres,], 1, median)

# filter by expression
jse_ind = which(jse > expr_thresh)
bse_ind = which(bse > expr_thresh)
if (strict_thresh) {
  all_ind = intersect(jse_ind, bse_ind)
} else {
  all_ind = union(jse_ind, bse_ind)
}

top_barres_filtered = top_in_barres[all_ind]
bse = bse[all_ind]
jse = jse[all_ind]

bsdf = data.frame(bse, jse, row.names = top_barres_filtered)


# filter by FC
if (filter_by_FC) {
  barres_genes = top_in_barres[which((bsdf[,1] / bsdf[,2]) >= fc_thresh)]
} else {
  barres_genes = top_in_barres
}

'
## COMPARE TO NEURON
top_in_barres = top_var[which(top_var %in% rownames(barres_df))]
bse = barres_df[top_in_barres, ]
# filter by min expression
if (strict_thresh) {
  bse = bse[rownames(bse)[min(bse[,1], bse[,2]) >= expr_thresh], ]
} else {
  bse = bse[rownames(bse)[max(bse[,1], bse[,2]) >= expr_thresh], ]
}
# filter by fc
barres_genes = rownames(bse)[(bse[,"FC"] >= fc_thresh)]
'
#####============----============#####

'
if (strict_consensus) {
  consensus_genes = intersect(consensus_genes, barres_genes)
} else {
  consensus_genes = union(consensus_genes, barres_genes)
}
'


# ~

#####======= ZHANG 2016 (data: "zhang_df", "zhang_read")  =======#####
# find common genes
# scale TPM w/ jt
# get signature and filter
#zhang_sig = signatureAll(zhang_df, astro_ids)
#####============----============#####

# ~
'
#####======= DULKEN 2017 (data: "ddf", "NSC_ids", "Astro_ids", "dulken_sig") =======#####
# filter by minimum expression (scale TPM??)
dsig = dulken_sig[as.vector(apply(dulken_sig[,c("meanA", "meanB")], 1, function(x){max(x) > expr_thresh})),]
# filter by minimum MEDIAN expression 

# filter by log2FC
dsig = dsig[dsig[,"log2FC"] >= log2fc_thresh,]
# filter by pval
dsig = dsig[dsig[,"pval"] < p_thresh,]
# list genes
dulken_genes = rownames(dsig)
#####======================----======================#####
#consensus_genes = intersect(consensus_genes, f_genes)
### NO STATISTICALLY DE BETWEEN ASTRO AND QNSC --> they confused astros and qNSCs (bad markers)
'
# ~


#####======= CORRELATION  =======#####
if (FALSE) {
  # normal jt expression by GM
  jt = archive_jt
  old_jt = archive_jt
  old_GMexprv_df = sapply(Sclusters, S_GMexprv)
  # constrained jt expression by GM
  jt = jt[consensus_genes, ]
  new_GMexprv_df = sapply(Sclusters, S_GMexprv)
  # correlate by GM
  cor_by_GM <- function(s) {
    # filter out NaN (and all 0?)
    NA_rows = which(sapply(1:dim(new_GMexprv_df)[1], function(x){any(is.na(new_GMexprv_df[x,]))}))
    # s is S cluster index
    if (length(NA_rows) > 0) {
      return(cor(old_GMexprv_df[-NA_rows,s], new_GMexprv_df[-NA_rows,s]))
    } else {
      return(cor(old_GMexprv_df[,s], new_GMexprv_df[,s]))
    }
  }
  # df of all cors 
  S_cor_df = sapply(1:4, cor_by_GM)
  # barplot 
  barplot(S_cor_df, beside = TRUE, xlab = "S cluster", ylab = "Pearson Correlation",
          names.arg = Snames, main = "S cluster consensus constrained correlation (Astro) \n All labels | Bobadilla '15")
  # add fc thresh, p val text
  log2fc_text = paste("log2FC threshold: ", as.character(log2fc_thresh))
  p_text = paste("p value threshold: ", as.character(p_thresh))
  expr_text = paste("min expression threshold: ", as.character(expr_thresh))
  cgene_text = paste("consensus genes: ", as.character(length(consensus_genes)))
  total_text = paste(paste(paste(paste(paste(paste(log2fc_text, "|", sep = " "), p_text, sep = " "), "|", sep = " "), cgene_text, sep = " "), "|", sep = " "), expr_text, sep = " ")
  #mtext(1.2, 0.5, log2fc_text)
  #mtext(1.2, 0.45, p_text)
  #mtext(1.2, 0.40, cgene_text)
  mtext(total_text, side = 1, cex = 0.6)
  
  ## CAUTION:
  # check consensus gene length
  # check "neuro" ids, check diff astro ids
  # use Z scores of correlations? 
  ## "run adjusted" setting
  ## plot of p value / fc and consensus gene number for each
  ## FILTER BY P or FC?? Give different correlations... (N only: S3 for pval, SA for FC)
  ## ctx: SA, str: S2 (for GP)
}
#####============----============#####

# ~

'
# GME rather than average
S_GME_pair <- function(m, s) {
  return(rowMeans(jt[gene_by_color(m),s]))
}
S_GME <- function(s) {
  return(sapply(ME_nums, S_GME_pair, s))
}
'


#####======= ITERATIVE CORRELATION  =======#####

# standardize data set gene names
#brg = barres_genes
bog = boba_genes
brg = barres_genes
# construct all desired iterations
if (strict_consensus) {
  consensus_genes = intersect(bog, brg)
} else {
  consensus_genes = union(bog, brg)
}

# filter qsig
#astro_consensus = consensus_genes
#consensus_genes = consensus_genes[!(consensus_genes %in% qsig_consensus)]
# weighted = TRUE
# astro_cor <--> astro_consensus
# new_cor <--> qfiltered consensus genes

cgenes = list(bog, brg, consensus_genes)
cnames = c("Boba", "Barres 14", "Boba & Barres 14")
c_ind = 1
for (c in cgenes) {
  # normal jt expression by GM
  jt = archive_jt
  old_jt = archive_jt
  old_GMexprv_df = sapply(Sclusters, S_GMexprv)
  # constrained jt expression by GM
  jt = jt[c, ]
  new_GMexprv_df = sapply(Sclusters, S_GMexprv)
  # get proportion of consensus genes by gene module
  cg_byGM = sapply(unique(net$colors), function(x){(sum(unlist(c) %in% gene_by_color(x)))/length(unlist(c))})
  # correlate by GM
  cor_by_GM <- function(s, weighted) {
    # filter out NaN (and all 0?)
    NA_rows = which(sapply(1:dim(new_GMexprv_df)[1], function(x){any(is.na(new_GMexprv_df[x,]))}))
    # s is S cluster index
    if (length(NA_rows) > 0) {
      if (weighted) {
        return(weightedCorr(old_GMexprv_df[-NA_rows,s], new_GMexprv_df[-NA_rows,s], weights = cg_byGM[-NA_rows]))
      } else {
        return(cor(old_GMexprv_df[-NA_rows,s], new_GMexprv_df[-NA_rows,s]))
      }
    } else {
      if (weighted) {
        return(weightedCorr(old_GMexprv_df[,s], new_GMexprv_df[,s], weights = cg_byGM))
      } else {
        return(cor(old_GMexprv_df[,s], new_GMexprv_df[,s]))
      }
    }
  }
  # df of all cors 
  S_cor_df = sapply(1:4, cor_by_GM, weighted)
  # build tot_cor_df
  if (c_ind == 1) {
    tot_cor_df = S_cor_df
  } else {
    tot_cor_df = rbind(tot_cor_df, S_cor_df)
  }
  '
  # barplot 
  par(mar = c(5, 5, 5, 5))
  main_text = paste("S cluster consensus constrained weighted correlation (qNSC) \n", cnames[c_ind], sep = " ")
  barplot(S_cor_df, beside = TRUE, xlab = "S cluster", ylab = "Pearson Correlation",
  names.arg = Snames, main = main_text) #Codega 14, Bobadilla 15, Dulken 17
  # add fc thresh, p val text
  fc_text = paste("FC threshold: ", as.character(fc_thresh))
  p_text = paste("p value threshold: ", as.character(p_thresh))
  expr_text = paste("min expression threshold: ", as.character(expr_thresh))
  cgene_text = paste("consensus genes: ", as.character(length(c)))
  total_text = paste(paste(paste(paste(paste(paste(fc_text, "|", sep = " "), p_text, sep = " "), "|", sep = " "), cgene_text, sep = " "), "|", sep = " "), expr_text, sep = " ")
  mtext(total_text, side = 1, cex = 0.6)
  '
  # advance index
  c_ind = c_ind + 1
}

rownames(tot_cor_df) = cnames


## figures text 
fc_text = paste("FC threshold: ", as.character(fc_thresh))
p_text = paste("p value threshold: ", as.character(p_thresh))
expr_text = paste("min expression threshold: ", as.character(expr_thresh))
cgene_text = paste("consensus genes: ", as.character(length(consensus_genes)))
weight_text = paste("weighted: ", as.character(weighted))
text_list = c(fc_text, p_text, expr_text, cgene_text, weight_text)
# make final text
total_text = ""
for (t in text_list) {
  total_text = paste(paste(t, "|", sep = " "), total_text, sep = " ")
}
#total_text = sapply(text_list, function(x){paste(paste(as.character(x), "|", sep = " "), total_text, sep = " ")})#function(x){paste(total_text, x, sep = " ")})

## heatmap 
## scale between three colors
#library(colorRampPalette)
par(mar = c(2, 3, 3, 3))
par(cex.main = 0.6)
col_vect = colorRampPalette(c("#428cf4", "white", "#f44242"))(1000) #"#428cf4", 
heatmap(tot_cor_df, Rowv = NA, Colv = NA, scale = "none", col = col_vect, labCol = Snames,
        main = "Astro CCGC by dataset", cexRow = 0.85)
# add text
mtext(total_text, side = 1, cex = 0.6, outer = FALSE)
# produced: consensus_qNSCvalid_corbydataset_heatmap.png

## barplot of consensus genes by data set combo
par(mar = c(13, 3, 3, 3), cex.main = 0.8)
cgene_lengths = sapply(cgenes, length)
barplot(cgene_lengths, names.arg = cnames, las = 2, main = "Consensus Gene Count by Dataset", ylab = "consensus genes")
# add text
mtext(total_text, side = 1, cex = 0.6, outer = FALSE)

# reset jt
jt = archive_jt
#####============----============#####

library(gplots)
library("RColorBrewer")
#par(mar = c(1, 1, 1, 1))
par(cex.main = 0.75)
heatmap.2((tot_cor_df), margins = c(5, 12), main = "Astro Correlation by Data Set", scale = "row", Rowv = NA, dendrogram = "none", labCol = c("S1", "S2", "S3", "SA"), col = brewer.pal(9, "Purples"), density.info = "none",  trace = "none", labRow = rownames(tot_cor_df), lmat = rbind(c(4, 3), c(2, 1)), lhei = c(0.4, 1), lwid = c(0.3, 1), cexRow = 1, cexCol = 1)
# astro_cor_df