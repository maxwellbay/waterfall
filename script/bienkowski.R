library(xlsx)

ant = read.csv("data/bienkowski/annotation.BrainCellAtlas_Saunders_version_2018.04.01.csv")
exp = read.csv("data/bienkowski/metacells.BrainCellAtlas_Saunders_version_2018.04.01.csv",row.names = 1)
mbt = read.xlsx2("/Users/maxwellbay/Projects/Waterfall/data/bienkowski/Supplemental Table 2. GeneAnnotation_updated_binary.xlsx",
                sheetIndex = 1)
rownames(mbt) = as.character(mbt[,1])

conversion = as.character(ant$full_name)
names(conversion) = as.character(ant$tissue_subcluster)

hippocam = grep("HC",ant$tissue_subcluster)
neuronal = grep("Neuron",ant$full_name)
non_ento = grep("ento",ant$common_name,ignore.case = TRUE,invert = TRUE)

ant_keep = ant[Reduce(intersect,list(hippocam,neuronal,non_ento)),]

keep_samps = conversion[intersect(grep("HC",names(conversion)),grep("Neuron",conversion))]

conv_sub = ant[ant$tissue_subcluster%in%names(keep_samps),]

colnames(exp) = sub("[.]","-",colnames(exp))
colnames(exp) = conversion[colnames(exp)]

exp.nrn = exp[,keep_idx]

mb.clst = Waterfall.Cluster(exp.nrn)
mb.cols = mb.clst[,1]

exp.nrn.rm = exp.nrn[,!colnames(exp.nrn)%in%c(rownames(mb.clst[mb.clst[,2]=="2",]))]

mb.clst.rm = Waterfall.Cluster(exp.nrn.rm, nClusters = 4)
mb.rm.cols = mb.clst.rm[,1]

mb.rm.pca = Waterfall.PCA(exp.nrn.rm,cols = mb.rm.cols)
mb.rm.tsne = Waterfall.tSNE(exp.nrn.rm,colors = mb.rm.cols,perplexity = 15,max_iter = 5000,pca = TRUE)


exp.nrn.rm.red = exp.nrn.rm[rownames(mbt)[toupper(rownames(mbt)) %in% toupper(rownames(exp.nrn.rm))],]

mb.clst.rm.red = Waterfall.Cluster(exp.nrn.rm.red,cell.cols = mb.rm.cols,nClusters = 3)
mb.clst.rm.red = Waterfall.Cluster(exp.nrn.rm.red,nClusters = 3)
mb.rm.red.cols = mb.clst.rm.red[,1]

mb.rm.red.pca.cs = Waterfall.PCA(exp.nrn.rm.red,cols = mb.rm.cols)
mb.rm.red.pca = Waterfall.PCA(exp.nrn.rm.red,cols = mb.rm.red.cols)



