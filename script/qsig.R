# 7/20/17: This file will serve as a summary of work to date on establishing a 
#           correspondence between S cluster and quiescence, and S cluster and gene modules.
#           Cross-comparing these correspondences will link quiescence to specific gene modules,
#           thereby establishing a "signature" of quiescence. 
#
#         The file contains functions common to all applications (which describe S and ME relations)
#         as well as identified genes which are upregulated in qNSCs across all 
#         data sets (consensus genes), and individual correlations between qNSC genes
#         and normal S clusters. 

library(RColorBrewer)

#####========= COMMON =========#####

# settings 
bulk = FALSE 
load_rdata = FALSE
fc_thresh = 6 #6
p_thresh = 0.01 #0.01
ftop = TRUE

if (!(load_rdata)) {
  # load priors
  source("load/LoadData.R")
  library("openxlsx")
  library("WGCNA")
  
  # Shin et al cluster objects (only q) and names
  Sclusters = list(S1.id, S2.id, S3.id, SAn.id)
  Snames = c("S1", "S2", "S3", "SA")
  jt = jt[, unlist(Sclusters)]
  
  # filter by top var
  if (ftop) {
    # determine top_var 
    gene_count = 5000
    top_var = apply(jt, 1, var)
    top_var = top_var[order(top_var, decreasing = TRUE)]
    top_var = names(top_var[1:gene_count])
    jt = jt[top_var,]
  }
  
  # create data expression (required for WGCNA_setup.R util)
  if (bulk) {
    # create bulk data expression 
    Sexprv <- function(s) {
      return(apply(jt[,s], 1, median))
    }
    Sexpr_df = sapply(Sclusters, Sexprv)
    colnames(Sexpr_df) = Snames
    datExpr = t(Sexpr_df)
  } else {
    # data expression is Shin et al data
    datExpr = t(jt)
  }
  
  # cluster data expression (produces "net" object)
  source("utils/WGCNA_setup.R")
  
  # load data sources
  ME_nums = sort(unique(net$colors))
  source("load/LoadData_Q.R")
  
  # save RData (dont save automatically, must do manually to avoid unwanted overwrite)
  #save.image("qsig.RData")
} else {
  # load RData of prior run
  load("qsig.RData")
  Sclusters = list(S1.id, S2.id, S3.id, SAn.id)
  Snames = c("S1", "S2", "S3", "SA")
}




# calc GM expr by S

avgGM_byS <- function(s, g) {
  return(mean(apply(jt[gene_by_color(g), s], 1, median)))
}
GM_allS <- function(g) {
  return(sapply(Sclusters, avgGM_byS, g))
}
GM_df = sapply(ME_nums, GM_allS)

# heatmap of GM expr by S
par(cex.main = 0.6)
heatmap(GM_df, Rowv = NA, scale = "column", col = brewer.pal(3, "Purples"), labRow = Snames, labCol = ME_nums,
        xlab = "Module Eigengene", ylab = "S Cluster", main = "Module Eigengene Mean Expression by S Cluster")
# me_meanexpr_byS_heatmap.png


# GM expression vector by S
S_GMexprv <- function(s) {
  return(sapply(ME_nums, function(x){avgGM_byS(s, x)}))
}


# calc ME value by S
avgME_byS <- function(s, m) {
  # m is ME index number
  if (is.numeric(m)) {
    ME_name = paste("ME", as.character(m), sep = "")
  } else { ME_name = m }
  S_ind = as.numeric(which(colnames(jt) %in% s))
  return(median(net$MEs[S_ind, ME_name]))
}

ME_allS <- function(m) {
  return(sapply(Sclusters, avgME_byS, m))
}

ME_df = sapply(ME_nums, ME_allS)

# heatmap of ME value by S
heatmap(ME_df, Rowv = NA, Colv = NA, scale = "column")

##### ================================ #####

# CONSENSUS DATABASE GENES (BELOW)

# not implemented

# INDIVIDUAL DATABASE CORRELATIONS (BELOW)

#####=== CODEGA 2014 (data frame: "cdf") ===#####
### PRE AND POST RESTRAINED CORRELATION 

# filter by p value and fold change 
cdgenes = rownames(cdf)[cdf[,"p-value"] < p_thresh & cdf[,"Fold Change"] >= fc_thresh]
# filter by Shin availability 
cdg = cdgenes[cdgenes %in% rownames(jt)]
# get indices of gene modules which contain cdg 
cdg_count = sapply(ME_nums, function(x){sum(cdg %in% gene_by_color(x))})
cdg_ind = which(cdg_count > 0)

# get expression of S cluster in cdg genes within specific GM
Sexpr_byGM <- function(m, s) {
  genes = cdg[cdg %in% gene_by_color(m)]
  if (length(genes) == 0) {
    return(0)
  } else {
    return(mean(apply(jt[genes, s], 1, median)))
  }
}

# get expression of S cluster in cdg genes across all GMs
Sexprv <- function(s) {
  return(sapply(ME_nums, Sexpr_byGM, s))
}

# correlate normal expression by GM and codega gene only expression by GM
#cor(GM_df[5,][cdg_ind], Sexprv(S5.id)[cdg_ind])
cor_norm_codega_expr <- function(s) {
  # s is S cluster name
  if (s == "SA") {
    s_ind = 4
    s_obj = SAn.id
  }
  if (s == "S1") {
    s_ind = 1
    s_obj = S1.id
  }
  if (s == "S2") {
    s_ind = 2
    s_obj = S2.id
  }
  if (s == "S3") {
    s_ind = 3
    s_obj = S3.id
  }
  return(cor(GM_df[s_ind,][cdg_ind], Sexprv(s_obj)[cdg_ind], method = "pearson"))
}

# calc correlation by S and barplot
S_codega_cors = sapply(Snames, cor_norm_codega_expr)
par(cex.main = 0.6)
barplot(S_codega_cors, ylab = "Pearson Correlation", xlab = "S cluster", main = "S Cluster Correlation between GM expression vector and Codega-restricted GM expression vector")
# codega_restricted_cor_byS_barplot.png 

'
# get the expression of cdg genes within a specific GM across a specific S cluster
S_cdg_expr_byGM <- function(m, s) {
  cdg_gm = cdg[cdg %in% gene_by_color(m)]
  if (length(cdg_gm) == 0) {
    return(0)
  }
  Scdg_expr = mean(apply(jt[cdg_gm, s], 1, median))
  return(Scdg_expr)
}

# get the expression of cdg genes across all GMs for a specific S cluster
S_cdg_exprv <- function(s) {
  return(sapply(ME_nums, S_cdg_expr_byGM, s))
}

# cdg df shows the mean expression of codega genes in each GM across all S clusters (row: GM, col: S cluster)
cdg_df = sapply(Sclusters, S_cdg_exprv)
heatmap(cdg_df, Rowv = NA, scale = "row", col = brewer.pal(3, "Purples"), labRow = rownames(cdg_df))

# ME df shows the value of eigengenes according to S cluster (row: S cluster, col: GM)
# GM df shows the expression of cdg genes by eigengene according to S cluster (row: S cluster, col: GM)

# correlate cdg to all S by expression values
#cor_cdg_allS_expr = cor(cdg_df[,], GM_df[1,]) 
'
'
### ME MEMBERSHIP COUNT (using ME_df)
# top 5 MEs for each S cluster
#S1_maxME = which(ME_df[1,] %in% sort(ME_df[1,], decreasing = TRUE)[1:2])
#S2_maxME = which(ME_df[2,] %in% sort(ME_df[2,], decreasing = TRUE)[1:2])
#S3_maxME = which(ME_df[3,] %in% sort(ME_df[3,], decreasing = TRUE)[1:2])
#SA_maxME = which(ME_df[4,] %in% sort(ME_df[4,], decreasing = TRUE)[1:2])
S1_maxME = which(ME_df[1,] > 0)
S2_maxME = which(ME_df[2,] > 0)
S3_maxME = which(ME_df[3,] > 0)
SA_maxME = which(ME_df[4,] > 0)

# count codega genes by top 5 ME for each S cluster
S1_count = sum(codega_gnames %in% unlist(sapply(S1_maxME, gene_by_color)))
S2_count = sum(codega_gnames %in% unlist(sapply(S2_maxME, gene_by_color)))
S3_count = sum(codega_gnames %in% unlist(sapply(S3_maxME, gene_by_color)))
SA_count = sum(codega_gnames %in% unlist(sapply(SA_maxME, gene_by_color)))
#SA_maxME = which(ME_df[4,] == max(ME_df[4,]))

# proportion of codega genes by genes in me
S1_prop = S1_count/(length(unlist(sapply(S1_maxME, gene_by_color))))
S2_prop = S2_count/(length(unlist(sapply(S2_maxME, gene_by_color))))
S3_prop = S3_count/(length(unlist(sapply(S3_maxME, gene_by_color))))
SA_prop = SA_count/(length(unlist(sapply(SA_maxME, gene_by_color))))

# not very useful: only S1 and S3 have an eval greater than zero, and its the same for both
'

##### =========================== #####




#####=== BOBADILLA 2014 (genes: "boba_c2g", "boba_c3g") ===#####
# look into: Boba overlap with Ruth (consensus genes) and compare to Codega for triple consensus 
# look into: restriction correlation method for Boba astrocyte genes
# look into: boba qNSC 1 vs qNSC 2 (different directionality of PT? qNSC 1 closer to astro, S1)
# look into: Boba and Codega consensus, Boba and Codega and Ruth consensus
# TRY: filter boba genes more aggressively (than FC 2 and padj < 0.05 -- try fc_thresh p_thresh)

### PRE AND POST RESTRAINED CORRELATION 
bdg = unique(c(boba_c2g, boba_c3g))
# consensus only genes
bdg_cdg_consensus = cdg[(cdg %in% bdg)]
bdg = bdg_cdg_consensus
# filter by Shin availability 
bdg = bdg[bdg %in% rownames(jt)]
# get indices of gene modules which contain bdg 
bdg_count = sapply(ME_nums, function(x){sum(bdg %in% gene_by_color(x))})
bdg_ind = which(bdg_count > 0)

# get expression of S cluster in bdg genes within specific GM
Sexpr_byGM <- function(m, s) {
  genes = bdg[bdg %in% gene_by_color(m)]
  if (length(genes) == 0) {
    return(0)
  } else {
    return(mean(apply(jt[genes, s], 1, median)))
  }
}

# get expression of S cluster in bdg genes across all GMs
Sexprv <- function(s) {
  return(sapply(ME_nums, Sexpr_byGM, s))
}

# correlate normal expression by GM and boba gene only expression by GM
#cor(GM_df[5,][cdg_ind], Sexprv(S5.id)[cdg_ind])
cor_norm_boba_expr <- function(s) {
  # s is S cluster name
  if (s == "SA") {
    s_ind = 4
    s_obj = SAn.id
  }
  if (s == "S1") {
    s_ind = 1
    s_obj = S1.id
  }
  if (s == "S2") {
    s_ind = 2
    s_obj = S2.id
  }
  if (s == "S3") {
    s_ind = 3
    s_obj = S3.id
  }
  return(cor(GM_df[s_ind,][bdg_ind], Sexprv(s_obj)[bdg_ind], method = "pearson"))
}

# calc correlation by S and barplot
S_boba_cors = sapply(Snames, cor_norm_boba_expr)
par(cex.main = 0.6)
#barplot(S_boba_cors, ylab = "Pearson Correlation", xlab = "S cluster", main = "S Cluster Correlation between GM expression vector and Boba-restricted GM expression vector")
# lesser trend due to less aggressive filtering (fc 2 padj < 0.05) in boba paper

barplot(S_boba_cors, ylab = "Pearson Correlation", xlab = "S cluster", main = "S Cluster Correlation between GM expression vector and Boba/Codega-restricted GM expression vector")

##### =========================== #####






#####=== BECKERVORDERSANDFORTH 2010 (genes: "rbd_ids") ===#####
# look into: Boba overlap with Ruth (consensus genes) and compare to Codega for triple consensus 
# look into: restriction correlation method for Boba astrocyte genes
# look into: boba qNSC 1 vs qNSC 2 (different directionality of PT? qNSC 1 closer to astro, S1)
# look into: Boba and Codega consensus, Boba and Codega and Ruth consensus
# TRY: filter boba genes more aggressively (than FC 2 and padj < 0.05 -- try fc_thresh p_thresh)

### PRE AND POST RESTRAINED CORRELATION 
bdg = unique(c(boba_c2g, boba_c3g))
# consensus only genes
bdg_cdg_consensus = cdg[(cdg %in% bdg)]
bdg = bdg_cdg_consensus
# filter by Shin availability 
bdg = bdg[bdg %in% rownames(jt)]
# get indices of gene modules which contain bdg 
bdg_count = sapply(ME_nums, function(x){sum(bdg %in% gene_by_color(x))})
bdg_ind = which(bdg_count > 0)

# get expression of S cluster in bdg genes within specific GM
Sexpr_byGM <- function(m, s) {
  genes = bdg[bdg %in% gene_by_color(m)]
  if (length(genes) == 0) {
    return(0)
  } else {
    return(mean(apply(jt[genes, s], 1, median)))
  }
}

# get expression of S cluster in bdg genes across all GMs
Sexprv <- function(s) {
  return(sapply(ME_nums, Sexpr_byGM, s))
}

# correlate normal expression by GM and boba gene only expression by GM
#cor(GM_df[5,][cdg_ind], Sexprv(S5.id)[cdg_ind])
cor_norm_boba_expr <- function(s) {
  # s is S cluster name
  if (s == "SA") {
    s_ind = 4
    s_obj = SAn.id
  }
  if (s == "S1") {
    s_ind = 1
    s_obj = S1.id
  }
  if (s == "S2") {
    s_ind = 2
    s_obj = S2.id
  }
  if (s == "S3") {
    s_ind = 3
    s_obj = S3.id
  }
  return(cor(GM_df[s_ind,][bdg_ind], Sexprv(s_obj)[bdg_ind], method = "pearson"))
}

# calc correlation by S and barplot
S_boba_cors = sapply(Snames, cor_norm_boba_expr)
par(cex.main = 0.6)
#barplot(S_boba_cors, ylab = "Pearson Correlation", xlab = "S cluster", main = "S Cluster Correlation between GM expression vector and Boba-restricted GM expression vector")
# lesser trend due to less aggressive filtering (fc 2 padj < 0.05) in boba paper

barplot(S_boba_cors, ylab = "Pearson Correlation", xlab = "S cluster", main = "S Cluster Correlation between GM expression vector and Boba/Codega-restricted GM expression vector")

#####============----============#####

# ~ 


#####======= DULKEN 2017 (data frame: "ddf") =======#####
# get fold changes and correct z scores
d_fcs = as.numeric(as.vector(ddf[,"Expression Fold Change"]))
d_zscores = as.numeric(as.vector(ddf[,"Corrected Z Score"]))
# filter 
dulken_z_thresh = 1
dulken_fc_thresh = 2
f_ind = which(d_zscores > dulken_z_thresh & d_fcs > dulken_fc_thresh)
f_genes = as.character(ddf[f_ind,][,1])

# find overlap with boba-codega consensus
consensus_genes = intersect(f_genes, intersect(bdg, cdg))
dg_count = sapply(ME_nums, function(x){sum(consensus_genes %in% gene_by_color(x))})
dg_ind = which(dg_count > 0)
# get expression of S cluster in bdg genes within specific GM
Sexpr_byGM <- function(m, s) {
  genes = f_genes[f_genes %in% gene_by_color(m)]
  if (length(genes) == 0) {
    return(0)
  } else {
    return(mean(apply(jt[genes, s], 1, median)))
  }
}
# get expression of S cluster in bdg genes across all GMs
Sexprv <- function(s) {
  return(sapply(ME_nums, Sexpr_byGM, s))
}
# correlate normal expression by GM and consensus gene only expression by GM
cor_norm_cons_expr <- function(s) {
  # s is S cluster name
  if (s == "SA") {
    s_ind = 4
    s_obj = SAn.id
  }
  if (s == "S1") {
    s_ind = 1
    s_obj = S1.id
  }
  if (s == "S2") {
    s_ind = 2
    s_obj = S2.id
  }
  if (s == "S3") {
    s_ind = 3
    s_obj = S3.id
  }
  return(cor(GM_df[s_ind,][dg_ind], Sexprv(s_obj)[dg_ind], method = "pearson"))
}
# calc correlation by S and barplot
S_cons_cors = sapply(Snames, cor_norm_cons_expr)
par(cex.main = 0.6)
#barplot(S_boba_cors, ylab = "Pearson Correlation", xlab = "S cluster", main = "S Cluster Correlation between GM expression vector and Boba-restricted GM expression vector")
# lesser trend due to less aggressive filtering (fc 2 padj < 0.05) in boba paper

barplot(S_cons_cors, ylab = "Pearson Correlation", xlab = "S cluster", main = "S Cluster Correlation between GM expression vector and BCD consensus restricted GM expression vector")

'
# barplot 
barplot(S_cor_df, beside = TRUE, xlab = "S cluster", ylab = "Pearson Correlation",
        names.arg = Snames, main = "S cluster consensus constrained correlation (Astro) \n All labels | Bobadilla 15")
# add fc thresh, p val text
log2fc_text = paste("log2FC threshold: ", as.character(log2fc_thresh))
p_text = paste("p value threshold: ", as.character(p_thresh))
expr_text = paste("min expression threshold: ", as.character(expr_thresh))
cgene_text = paste("consensus genes: ", as.character(length(consensus_genes)))
total_text = paste(paste(paste(paste(paste(paste(log2fc_text, "|", sep = " "), p_text, sep = " "), "|", sep = " "), cgene_text, sep = " "), "|", sep = " "), expr_text, sep = " ")
#mtext(1.2, 0.5, log2fc_text)
#mtext(1.2, 0.45, p_text)
#mtext(1.2, 0.40, cgene_text)
mtext(total_text, side = 1, cex = 0.6)
'
#####============----============#####

# ~ 