
## search names (make util)

firstup <- function(x) {
  substr(x, 1, 1) <- toupper(substr(x, 1, 1))
  return(x)
}

find_in_jt <- function(g) {
  # case insensitive search in jt
  return(firstup(tolower(g)) %in% rownames(jt))
}

find_in_jt_portion <- function(g) {
  # find partial names
  n1 = rownames(jt)[sapply(rownames(jt), function (x, y) {grepl(y, x)}, g)]
  n2 = rownames(jt)[sapply(rownames(jt), function (x, y) {grepl(y, x)}, firstup(tolower(g)))]
  return(c(n1, n2))
}

# astro marker genes
#amarkers = c("gfap", "aldh1l1", "s100b", "aqp4", "serpinf1", 
#             "gja1", "mfge8", "slco1c1")
amarkers = c("gfap", "aldh1l1", "s100b", "aqp4", "gja1", "mfge8", "slco1c1")
Sclusters = list(S1.id, S2.id, S3.id, S4.id, S5.id, SAn.id)

# only type 1 dominant amarkers (from Zeisel)
#amarkers = c("aldh1l1", "gfap", "aqp4", "serpinf1")

# only type 2 dominant amarkers (from Zeisel)
#amarkers = c("gja1", "mfge8", "slco1c1")

# some from Zeisel et al. '15 -- distinguish type 1 and type 2?

gene_across_allS <- function(gn) {
  g = firstup(tolower(gn))
  gene_byS <- function(s, g) {return(mean(rowMeans(jt[g, s])))}
  return(sapply(Sclusters, gene_byS, g))
}

# bar plot of known astro markers
Snames = c("S1", "S2", "S3", "S4", "S5", "SA")
amarker_df = as.data.frame(sapply(amarkers, gene_across_allS))

barplot(as.matrix(amarker_df), beside=TRUE, legend = Snames, ylab = "Average Marker Expression [TPM]", xlab = "Astrocyte Marker", main = "Astrocyte Marker Expression by S Cluster", bty = "n")
heatmap(as.matrix(amarker_df), scale = "column", labRow = Snames, col = brewer.pal(4, "Purples"), main = "Astrocyte Marker Expression by S Cluster", ylab = "S Cluster", xlab = "Astrocyte Marker",
        margins = c(6, 4))
'
# scale for barplot
amarker_scaled = apply(amarker_df, 2, function(x){(x)/sum(x)})
par(mfrow = c(1, 1), mar = c(5, 5, 4, 4), xpd = TRUE)
barplot(as.matrix(amarker_scaled), beside=TRUE, legend = Snames, args.legend = list(x = "topright", bty = "n", inset = c(-0.1, 0)),
          ylab = "Fraction of Total Average Marker Expression", xlab = "Astrocyte Marker", main = "Astrocyte Marker Expression by S Cluster")
# use div seq data fold change / sig all (lucien data) to identify astro upregulated
  
# scale amarker expression by amarker
amarker_scaled_col = apply(amarker_df, 2, function(x){x / sum(x)})
am_scaled_col_allS = rowMeans(amarker_scaled_col)
print(am_scaled_col_allS)

# NOTES
# peaks at S2 and SA, decreasing from S1 -- maybe S1 --> S2 --> SA astrogenesis
# type 1 dominant: SA and S2 prominent 
# zeisel: type 1 by Gfap --> SA is highest Gfap expr
# type 2 dominant: S1, S2, SA prominent
# zeisel: type 2 by Mfge8 --> S2 is highest Mfge8 expr
# draw trajectories by differences going from S1 or other state
# find most consistent trajectories and starting state (increasing expression path)
# > diff neuron types in S3->S5 neurogenesis trajectory?

# type 1 and type 2 trajectories by Mfge8, Gfap
exc_amarkers = c("Mfge8", "Gfap")
exc_am_df = sapply(exc_amarkers, gene_across_allS)
heatmap(exc_am_df, scale = "col", Rowv = NA)
barplot(as.matrix(exc_am_df), beside=TRUE, legend = Snames, args.legend = list(x = "topright", bty = "n", inset = c(-0.1, 0)),
        ylab = "Average Marker Expression by S Cluster", xlab = "Exclusive Astrocyte Marker", main = "Exclusive Astrocyte Marker Expression by S Cluster")
# add: Sox21, Dbx2, Sox3?


# show all Zeisel AM, only type 1 and only type 2 by marker average across S clusters
t1_amarkers = c("aldh1l1", "gfap", "aqp4") 
# excluded serpinf1
t2_amarkers = c("gja1", "mfge8", "slco1c1")
all_amarkers = c(t1_amarkers, t2_amarkers) #c("gfap", "aldh1l1", "s100b", "aqp4", "gja1", "mfge8", "slco1c1")

scale_by_col <- function(d){
  return(apply(d, 2, function(x){x / sum(x)}))
}

t1_df = sapply(t1_amarkers, gene_across_allS)
t2_df = sapply(t2_amarkers, gene_across_allS)
all_df = sapply(all_amarkers, gene_across_allS)

# scale dfs 
t1_scaled_df = scale_by_col(t1_df)
t2_scaled_df = scale_by_col(t2_df)
all_scaled_df = scale_by_col(all_df)

# create mean scaled amarker vectors
t1_vec = rowMeans(t1_scaled_df)
t2_vec = rowMeans(t2_scaled_df)
all_vec = rowMeans(all_scaled_df)

barplot(cbind(t1_vec, t2_vec, all_vec), beside = TRUE, names.arg = c("Type I", "Type II", "Both Types"), ylab = "Average Proportion of Astro Marker Expression",
        main = "Average Proportion of Astrocyte Marker Expression by S Cluster Across Type Markers", legend = Snames, args.legend = list(x = "topright", bty = "n", inset = c(-0.15, 0)))
'

## directionality of markers
decreasing_expr_order_marker <- function(m) {
  # l is limit: max is 6, 3 would show top 3 decreasing nodes
  return(Snames[order(gene_across_allS(m), decreasing = TRUE)])
}

# get all decreasing relations in a data frame
di_df = sapply(amarkers, decreasing_expr_order_marker)
ddf = di_df[1:3,]

'
## adjacency matrix of direction
# 7/17/17: hold off on this for now, no obvious application of adjacency matrix rn
# make adjacency matrix (no edge identity) of multidigraph from above
# get name: sapply(ddf[,1], function(x){which(x == Snames)})
ind_mat = matrix(sapply(ddf, function(x){which(Snames == x)}), nrow = 3)
# build paths:
path_col <- function(c) {
  # c is ind_mat column index
  path_ind = ind_mat[,c]
  p1 = path_ind[1]
  p2 = path_ind[2]
  p3 = path_ind[3]
  # determine ordering
  # returns a column in adjacency matrix
}
'

'
##### CAHOY 2008 (Barres) ####
cahoy_astro_read = read.xlsx("data/340_Cahoy_S_Table_S4_AvX_2007-09-11.xls", 1)
cahoy_astro_df = as.data.frame(cahoy_astro_read)
colnames(cahoy_astro_df) = cahoy_astro_df[1,]
cahoy_astro_df = cahoy_astro_df[,c(3, 4)]
rownames(cahoy_astro_df) = cahoy_astro_df[,1]
cahoy_astro_df = cahoy_astro_df[2:dim(cahoy_astro_df)[1],]
cahoy_astro_gnames = rownames(cahoy_astro_df)[rownames(cahoy_astro_df) %in% rownames(jt)]
cahoy_astro_df = cahoy_astro_df[cahoy_astro_gnames, ]
# naturally filtered by FC >= 1.5, could filter more if necesssary 
# filter by top_var membership
gene_count = 5000
top_var = apply(jt, 1, var)
top_var = top_var[order(top_var, decreasing = TRUE)]
top_var = names(top_var[1:gene_count])
cahoy_astro_gnames = cahoy_astro_gnames[cahoy_astro_gnames %in% top_var]
cahoy_astro_df = cahoy_astro_df[cahoy_astro_gnames,]
# filter by FC
thresh = 20
cahoy_astro_gnames = cahoy_astro_gnames[as.numeric(cahoy_astro_df[,2]) > thresh]
cahoy_astro_df = cahoy_astro_df[cahoy_astro_gnames,]
# filter by expression level
cahoy_astro_gnames = cahoy_astro_gnames[apply(jt[cahoy_astro_gnames,c(S1.id, S2.id, S3.id, SAn.id)], 1, max) > 100]
cahoy_astro_df = cahoy_astro_df[cahoy_astro_gnames,]

# filter by S4 / S5 expression
Sactive_expr <- function(g) {
  return(max(as.numeric(jt[g, c(S4.id, S5.id)])))
}
Sinactive_expr <- function(g) {
  return(max(as.numeric(jt[g, c(S1.id, S2.id, S3.id, SAn.id)])))
}
# filter genes by comparing active and inactive enrichment
filter_byactive <- function(g) {
  active = Sactive_expr(g)
  inactive = Sinactive_expr(g)
  if (active >= (0.1)*inactive) {
    return(FALSE)
  } else {
    return(TRUE)
  }
}

# filtered astro genes by expression
cahoy_filtered_genes = cahoy_astro_gnames[sapply(cahoy_astro_gnames, filter_byactive)]
cahoy_astro_gnames = cahoy_filtered_genes
cahoy_astro_df = cahoy_astro_df[cahoy_astro_gnames, ]

avg_cahoy_by_S <- function(s) {
  # s is an object (e.g. S1.id)
  return(mean(rowMeans(jt[cahoy_astro_gnames, s])))
}

# mean and median give diff results without sufficiently aggessive active / inactive filtering
# S1 comes up as top in mean of above unless sufficient filtering


cahoyv_by_S <- function(s) {
  # s is an object (e.g. S1.id)
  return(rowMeans(jt[cahoy_astro_gnames, s]))
}

cahoy_df = sapply(Sclusters, cahoyv_by_S)
heatmap(as.matrix(cahoy_df), scale = "row", labRow = cahoy_astro_gnames, col = brewer.pal(9, "Purples"), main = "Astrocyte Marker Expression by S Cluster", ylab = "S Cluster",
        xlab = "Astrocyte Marker", margins = c(6, 4))

print(apply(cahoy_df, 2, mean))

## CONCLUSIONS (7/18): Basically different thresholds, filters, etc. can be applied
#                     to cause S1, S2 or SA to come up as "most astrogenic" 
#                     note that the astro markers which are most enriched tend to be exclusive
#                     TRY: change in developmental markers

##### CAHOY 2008 (Barres) - DEVELOPMENTAL ####
cd_astro_read = read.xlsx("data/450_Cahoy_S_Table_S15_AYvAO_down_2007-09-11.xls", 1)
cd_astro_df = as.data.frame(cd_astro_read)
colnames(cd_astro_df) = cd_astro_df[1,]
cd_astro_df = cd_astro_df[2:dim(cd_astro_df)[1],]
rownames(cd_astro_df) = cd_astro_df[,3]
cd_astro_df = cd_astro_df[,c(3, 4)]
astrod_gnames = rownames(cd_astro_df)

# filter by FC
# filter by top_var
astrod_gnames = astrod_gnames[astrod_gnames %in% top_var]

# filtered astro genes by expression
astrod_gnames = astrod_gnames[sapply(astrod_gnames, filter_byactive)]


# show expression by S cluster
astrod_expr_S <- function(s) {
  return(mean(as.numeric(apply(jt[astrod_gnames, s], 1, median))))
}
astrodv_S <- function(s) {
  return(as.numeric(apply(jt[astrod_gnames, s], 1, median)))
}

astrod_df = sapply(Sclusters, astrodv_S)
nonzero_rows = which(!(apply(astrod_df, 1, function(x){all(x == 0)})))
astrod_gnames = astrod_gnames[nonzero_rows]
astrod_df = astrod_df[nonzero_rows, ]
heatmap(as.matrix(astrod_df), scale = "row", labRow = astrod_gnames, col = brewer.pal(9, "Purples"), main = "Astrocyte Marker Expression by S Cluster", ylab = "S Cluster",
        xlab = "Astrocyte Marker", margins = c(6, 4), Colv = NA)

## CONCLUSION: Too close, not informative
'


##### DULKEN 2017 ####
# load in genes regulated differentially in astrocytes compared to qNSC's
Sclusters = list(S1.id, S2.id, S3.id, S4.id, S5.id, SAn.id)
# compare only between S1-3, SA?
d_astro_read = read.xlsx("data/dulken2017-mmc6.xlsx", 1)
d_astro_df = as.data.frame(d_astro_read)
colnames(d_astro_df) = d_astro_df[1,]
d_astro_df = d_astro_df[2:dim(d_astro_df)[1],]
rownames(d_astro_df) = d_astro_df[,1]
d_astro_df = d_astro_df[,c(2, 3, 4)]
d_astro_df = d_astro_df[rownames(d_astro_df)[rownames(d_astro_df) %in% top_var], ]
# genes upregulated in astrocytes relative to qNSC's
d_astrog = rownames(d_astro_df)[as.numeric(d_astro_df[,1]) < -4]
d_astro_df = d_astro_df[d_astrog, ]

dexpr_byS <- function(s) {
  return(mean(apply(jt[d_astrog, s], 1, median)))
}
dexprv_byS <- function(s) {
  return(apply(jt[d_astrog, s], 1, median))
}

dexpr_df = sapply(Sclusters, dexprv_byS)
dexpr_df = dexpr_df[!(apply(dexpr_df, 1, function(x){all(x == 0)})),]
heatmap(as.matrix(dexpr_df), scale = "row", labRow = d_astrog, col = brewer.pal(9, "Purples"), main = "Astrocyte Marker Expression by S Cluster", ylab = "S Cluster",
        xlab = "Astrocyte Marker", margins = c(6, 4))
