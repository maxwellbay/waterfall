## This file compares the known transcriptomes of NSC's (Shin) and Neurons and Astrocytes (Stanford Brain seq), 
## and plots their continuity on a PCA graph. Hopefully it will show the directionality of development from qNSC to 
## astrocyte and to neuron, if such a mechanism exists.

options(java.parameters = "-Xmx1024m")
library("GEOquery")
library("ggplot2")
library("ggfortify")
library("openxlsx")
source("src/HyperGeoTerms.R")
source("src/Waterfall.R")
# stop loading plots
pdf(file = NULL)
source("load/LoadData.R")
# restart loading plots
dev.off()
options(device = "RStudioGD")

# read in Astro/Neuron data (in FPKM)
ans <- read.xlsx('data/barreslab_rnaseq.xlsx', 1)
ansdf <- (as.data.frame(ans))

# filter by availability in jt (Shin data)
ansdf = ansdf[ansdf$Gene.symbol %in% rownames(jt),]

# make astrocyte data frame
astrodf = data.frame(ansdf$Astrocytes)
rownames(astrodf) = ansdf$Gene.symbol
colnames(astrodf) = "Astrocytes"

# filter jt by availability in astro (Barres lab seq data)
jt = jt[rownames(jt) %in% ansdf$Gene.symbol,]
print(dim(ansdf))
print(dim(jt))
print("DIMENSIONS")

# make neuron data frame
neurodf = data.frame(ansdf$Neuron)
rownames(neurodf) = ansdf$Gene.symbol
colnames(neurodf) = "Neuron"


# function to plot pca of transcriptomes
pca_plot <- function(ds, label=FALSE) {
  ## input is a gene expression data frame (e.g. jt)
  # filter NA cols
  ds = ds[,colnames(ds)[!(grepl("NA", colnames(ds)))]]
  # filter all zero cols
  ds = ds[,colnames(ds)[unlist(lapply(colnames(ds), function(x) {!(all(ds[,x] == 0))}))]]
  ds = ScaleTPM(ds)
  # compute pca and label cells by cluster with "vc" column
  ds.pca = prcomp(ds, center = TRUE, scale = TRUE)
  vc = rep("none", length(rownames(ds)))
  ds = cbind(ds, vc)
  ds$"vc" <- as.character(ds$"vc")
  ds[(rownames(ds) %in% S1.id), ] = apply(ds[(rownames(ds) %in% S1.id), ], 1, function (x) {x["vc"] = "S1"})
  ds[(rownames(ds) %in% S2.id), ] = apply(ds[(rownames(ds) %in% S2.id), ], 1, function (x) {x["vc"] = "S2"})
  ds[(rownames(ds) %in% S3.id), ] = apply(ds[(rownames(ds) %in% S3.id), ], 1, function (x) {x["vc"] = "S3"})
  ds[(rownames(ds) %in% S4.id), ] = apply(ds[(rownames(ds) %in% S4.id), ], 1, function (x) {x["vc"] = "S4"})
  ds[(rownames(ds) %in% S5.id), ] = apply(ds[(rownames(ds) %in% S5.id), ], 1, function (x) {x["vc"] = "S5"})
  ds[(rownames(ds) %in% SAn.id), ] = apply(ds[(rownames(ds) %in% SAn.id), ], 1, function (x) {x["vc"] = "SAn"})
  ds["Neuron",]["vc"] = "Neuron"
  ds["Astrocytes",]["vc"] = "Astro"
  print(autoplot(ds.pca, data = ds, colour = "vc", loadings.label = label))
  return(ds)
}

# prep and plot data frames
#jt = t(jt)
#nadf = cbind(neurodf, astrodf)
#nadf = t(nadf)
#total <- rbind(nadf, jt)
#pca_df = pca_plot(total)

# bind jt and nadf
common_genes = rownames(nadf)[rownames(nadf) %in% rownames(jt)]
all_df = cbind(nadf[common_genes,], jt[common_genes,])

# mean by S cluster
Sclusters = list(S1.id, S2.id, S3.id, S4.id, S5.id, SAn.id)
avg_byS <- function(s) {
  return(rowMeans(all_df[common_genes, s]))
}

# construct average S df
#avgS_df = as.data.frame(avg_byS(S1.id))
#avgS_df = cbind(avgS_df, as.data.frame(avg_byS(S2.id)))

## EUCLIDEAN DISTANCE 

# check t-ome distance N-A versus S
pt_cor <- function(s, t) {
  typeg = nadf[common_genes, t]
  popg = avg_byS(s)
  return(cor(typeg, popg))
}

pt_dist <- function(s, t) {
  typeg = nadf[common_genes, t]
  popg = avg_byS(s)
  return(dist(rbind(t(typeg), t(popg))))
}

## PCA EUCLIDEAN DISTANCE

all_df.pca = prcomp(t(all_df))
all_pca = all_df.pca$x
pca1 = all_pca[, 1]
pca2 = all_pca[, 2]

pt_dist_pca <- function(p, t) {
  meanp = c(mean(pca1[p]), mean(pca2[p]))
  meant = c(pca1[t], pca2[t])
  return(dist(rbind(t(meanp), t(meant))))
}

pt_cor_pca <- function(p, t) {
  meanp = c(mean(pca1[p]), mean(pca2[p]))
  meant = as.vector(c(pca1[t], pca2[t]))
  return(cor(meanp, meant))
}

# always just say Neuro is S4 or S3 ... bad Barres lab data? Get something
# closer?
