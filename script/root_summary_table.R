library(tidyverse)
library(formattable)


df <- data.frame("Phenotype" = c("NSC Number","NSC Activation","Neurogenesis"),
                 "Aging" = c(-1,-1,-1),
                 "SS" = c(1,1,1))

formattable(df,
            list(`Aging`= formatter("span", 
                                           style = ~ style(color = ifelse(`Aging` > 0, "green", "red")),                                    
                                           ~ icontext(sapply(`Aging`, function(x) if (x < 0) "arrow-down" else if (x > 0) "arrow-up" else "") )),
                 `SS`= formatter("span", 
                                    style = ~ style(color = ifelse(`SS` > 0, "green", "red")),                                    
                                    ~ icontext(sapply(`SS`, function(x) if (x < 0) "arrow-down" else if (x > 0) "arrow-up" else "") ))
                 ),
            align = c("l","c","c"))


  
  
