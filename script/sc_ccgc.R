# 8/7/17: The purpose of this file is to combine the sconstrained correlation method
#         with CCGC.

source("load/LoadData.R")
source("src/HyperGeoTerms.R")
source("load/LoadData_Astro.R")

# settings
Sclusters = list(S1.id, S2.id, S3.id, S4.id, S5.id, SAn.id)
p_thresh = 0.05


### S CONSTRAINED DE ###

DE_pairwise_S <- function(s1, s2, df, p_thresh) {
  # s1, s2 are Scluster indices
  print(paste(paste("pairwise:", as.character(s1), sep = " "), as.character(s2), sep = " "))
  pdf = sapply(rownames(df), function(x){t.test(as.numeric(df[x, Sclusters[[s1]]]), as.numeric(df[x, Sclusters[[s2]]]))$p.value})
  return(rownames(df)[which(pdf < p_thresh)])
}

get_DE_pairwise_allS <- function(df, p_thresh) {
  ind_comb = combn(1:length(Sclusters), 2)
  pw_comb = apply(ind_comb, 2, function(x){DE_pairwise_S(x[1], x[2], df, p_thresh)})
  # sort by S ind
  s_inds = 1:length(Sclusters)
  sgenes = list()
  for (s in s_inds) {
    print(paste("starting", as.character(s), sep = " "))
    col_inds = which(apply(ind_comb, 2, function(x){s %in% x}))
    cgenes = vector()
    # get consensus for this S cluster
    for (ci in col_inds) {
      if (length(cgenes) == 0) {
        cgenes = pw_comb[[ci]]
      } else {
        cgenes = intersect(cgenes, pw_comb[[ci]])
      }
    }
    # save consensus for S
    if (length(sgenes) == 0) {
      sgenes = list(cgenes) 
    } else {
      sgenes = append(sgenes, list(cgenes))
    }
  }
  return(sgenes)
}

ngs_pairwise = get_DE_pairwise_allS(jt, p_thresh)
cgs_pairwise = sapply(ngs_pairwise, function(x){length(unlist(x))})

save.image("sc_ccgc.RData") # full jt, full Sclusters

### TYPE DE ###
## for astro, just load asig? no -- look at type DE relative to 

type_ids = list(boba_astro_ids, boba_neuro_ids)

DE_type_S <- function(s, type_ind, df, p_thresh) {
  # s1, s2 are ind
  print(paste(paste("type pairwise:", as.character(s), sep = " "), as.character(type_ind), sep = " "))
  pdf = sapply(rownames(df), function(x){t.test(as.numeric(df[x, Sclusters[[s]]]), as.numeric(df[x, type_ids[[type_ind]]]))$p.value})
  return(rownames(df)[which(pdf < p_thresh)])
}

DE_type_allS <- function(type_ind, df, p_thresh, S_ind) {
  DE_type = lapply(S_ind, DE_type_S, type_ind, df, p_thresh)
  cgenes = vector()
  for (d in DE_type) {
    if (length(cgenes) == 0) {
      cgenes = unlist(d)
    } else {
      cgenes = intersect(cgenes, unlist(d))
    }
  }
  return(list(cgenes))
}



# get all DE genes for type (pairwise) -- EXCLUDE specific S in df
# e.g. 
# astro_g_S1 = DE_type_allS(1, jt[,colnames(jt)[!(colnames(jt) %in% S1.id)]], p_thresh)
# S_DE = get_DE_pairwise_allS(jt, p_thresh)
# astro_S1 = intersect(S_DE[[1]], astro_g_S1)
# return(cor(jt[astro_S1, S1.id], bdf[astro_S1, type_ids[type_ind]]))

### CORRELATE ###

#S_DE = get_DE_pairwise_allS(jt, p_thresh)

# could have type S DE genes for easy

master_cor_S <- function(s, df, type_ind, S_DE, type_DEs) {
  # s is S cluster index
  not_s = (1:length(Sclusters))[-s]
  #type_g_S = DE_type_allS(type_ind, df, p_thresh, not_s)
  #type_g_S = type_DEs[s,type_ind][[1]]
  #type_g_S = type_DEs[[type_ind]]
  # exclusive others 
  #type_g_S = Reduce(intersect, type_DEs[not_s,type_ind])
  # exclusive self
  #type_g_S = unlist(type_DEs[s,type_ind])
  # inclusive self
  #type_g_S = rownames(df)[!(rownames(df) %in% unlist(type_DEs[s,type_ind]))]
  type_g_S = rownames(df)
  # intersect S DE and type DE
  #type_S = intersect(S_DE[[s]], type_g_S)
  # type DE only
  type_S = type_g_S
  # S DE only
  #type_S = S_DE[[s]]
  #type_S = intersect(rownames(df), type_S)
  print(length(type_S))
  print(sum(type_S %in% rownames(df)))
  
  #print(length(type_S))
  #ret_val = dist(rbind(as.numeric(rowMeans(df[type_S, Sclusters[[s]]])), as.numeric(rowMeans(df[type_S, type_ids[[type_ind]]]))), method = "euclidean")[1]
  #print(ret_val)
  ret_val = cor(as.numeric(apply(df[type_S, Sclusters[[s]]], 1, median)), as.numeric(apply(df[type_S, type_ids[[type_ind]]], 1, median)), method = "pearson")
  print(ret_val)
  return(ret_val)
}

# combine data frames (boba)
#### TRY BOBA DF not filtered by top var, so can use lower p val and correlation
combined_genes = intersect(rownames(jt), rownames(ubdf))
combined_df = cbind(jt[combined_genes,], ubdf[combined_genes,])

type_p_thresh = 0.05

# make type DEs
tds = sapply((1:length(type_ids)), function(x){sapply((1:length(Sclusters)), DE_type_S, x, combined_df, type_p_thresh)})
#td = apply(tds, 2, function(x){Reduce(intersect, x)})
#td = sapply((1:length(type_ids)), DE_type_allS, combined_df, type_p_thresh, 1:length(Sclusters))
#type_DEs = sapply((1:length(type_ids)), function(y){sapply(1:length(Sclusters), function(x){DE_type_allS(y, combined_df, type_p_thresh, (1:length(Sclusters))[-x])})})
# type_DEs shows genes by s cluster and type such that the type has a significant difference when compared to ALL clusters except that S cluster

# k = Reduce(intersect, l)

### could do with combinations - would be faster
## correlate union of DEs?
## optimize type pairwise ... very silly implementation, dont need to recalculate all(1:length(type_ids))(1:length(type_ids))(1:length(type_ids))(1:length(type_ids))(1:length(type_ids))v       
#astro_DE = DE_type_allS(1, combined_df, p_thresh, 1:length(Sclusters))

# boba astro
master_cor_astro = sapply(1:length(Sclusters), master_cor_S, combined_df, 1, ngs_pairwise, tds)
master_cor_neuro = sapply(1:length(Sclusters), master_cor_S, combined_df, 2, ngs_pairwise, tds)

# try distance rather than cor?