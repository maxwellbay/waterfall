source("src/Waterfall.R")
source("load/LoadData.R")

loadShinGenesMm9Star = function(new=FALSE){
  unit = "TPM"
  
  shin_data_genes = file.path("data","shin_mm9_star.csv")
  #generates expression matrix - genes
  if ((!file.exists(shin_data_genes))|new){
    files = list.files("data/shin_mm9_star/genes")
    for (i in 1:length(files)){
      file_name = files[i] 
      samp_name = file_name
      path_to_file = file.path("data","shin_mm9_star","genes",file_name)
      
      tmp = as.data.frame(read.table(path_to_file,header = TRUE))
      
      expr = as.numeric(tmp[,unit])
      names(expr) = as.character(tmp[,"gene_id"])
      
      if (i==1){
        all.mat = as.data.frame(matrix(expr,nrow = nrow(tmp),ncol=1))
        rownames(all.mat) = names(expr)
        colnames(all.mat) = samp_name
        
      }else{
        all.mat[names(expr),samp_name] = expr
      }
    }
    
    write.csv(all.mat,shin_data_genes)
  }
  
  #load all data
  return(as.data.frame(as.matrix(read.table(shin_data_genes,header = TRUE,row.names = 1,sep = ","))))
  
}

#load data
jt.gns = loadShinGenesMm9Star() #assign 'new = TRUE' to generate new expression table

#replace colnames 
srr_tbl = read.table("data/shin_mm9_star/association.txt",header = TRUE,row.names = 1)

new_cols = as.character(srr_tbl[unlist(lapply(strsplit(colnames(jt.gns),split = "[.]"),function(l)return(l[1]))),6])
colnames(jt.gns) = new_cols


#get cols from Shin et all alignment
jt.gns.cont = jt.gns[,!colnames(jt.gns)%in%colnames(jt)]
jt.gns = jt.gns[,colnames(jt)]
jt.gns.cols = jt.cols
jt.gns = cbind(jt.gns,jt.gns.cont)
jt.gns.cols = c(jt.gns.cols,rep("purple",ncol(jt.gns.cont)))
names(jt.gns.cols) = colnames(jt.gns)


jt.grx = Waterfall.Cluster(jt.gns,cell.cols = jt.gns.cols,nClusters = 6)

#PCA
jt.pca = Waterfall.PCA(jt,cols = jt.cols,plot_title = "PCA | Shin (Original)")
jtmm9.pca = Waterfall.PCA(jt.gns,cols = jt.gns.cols,plot_title = "PCA | Shin (mm9 - RefSeq - STAR)")




