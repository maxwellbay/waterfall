library(xlsx)

dat = as.matrix(read.xlsx2(file = "/Users/maxwellbay/Desktop/usc/seq_runs/test3/read_summary.xlsx",sheetIndex = 1,row.names =1,header = TRUE))
storage.mode(dat) = "numeric"
dat = dat[order(as.numeric(dat[,3])),]

ss_grps = c("SS1","SS2","SS4")
lc_grps = c("USC","CHLA")
ix_grps = c("nextera")

#total reads by smrt grouop
ss_clrs = rep("black",nrow(dat)); names(ss_clrs) = rownames(dat)
ss_clrs[grep(ss_grps[2],names(ss_clrs))] = "green"
ss_clrs[grep(ss_grps[3],names(ss_clrs))] = "red"

x = barplot(log2(as.numeric(dat[,3])),col = ss_clrs,main = "Total Reads by Smrt Group",ylab = "Total Reads",xlab = "Sample")
legend("topleft",legend = c("SS1","SS2","SS4"),fill = c("black","green","red"))
text(cex=.35, x=x-.75, y=-(max(dat[,3])/13), names(ss_clrs), xpd=TRUE, srt=70)

#total reads by pooling factor 
min_thresh = .5
max_thresh = 10
pool_dat = as.data.frame(dat[grep("nextera",rownames(dat),invert = TRUE),])
pool_dat$relative_value = 2^(pool_dat$ct-mean(pool_dat$ct))
pool_dat$factor_true = 1/pool_dat$relative_value
pool_dat$factor = pool_dat$factor_true
pool_dat$factor[pool_dat$factor<min_thresh] = min_thresh
pool_dat$factor[pool_dat$factor>max_thresh] = max_thresh

pool_clrs = ss_clrs[rownames(pool_dat)]

#plot total reads as a funciton of utilized factor (log scale)
plot(pool_dat$factor,log2(pool_dat$Total.Reads),col = pool_clrs,pch=19,cex=2)

#plot total reads as a funciton of relative value
plot(pool_dat$ct,pool_dat$Total.Reads,col = pool_clrs,pch=19,cex=1.5)

range_pch = rep(13,nrow(pool_dat))
range_pch[which(pool_dat$factor_true==pool_dat$factor)] = 19
plot(pool_dat$relative_value[-which.max(pool_dat$relative_value)],log2(pool_dat$Total.Reads[-which.max(pool_dat$relative_value)]),
     col = pool_clrs[-which.max(pool_dat$relative_value)],pch=range_pch[-which.max(pool_dat$relative_value)],cex=1.5)



