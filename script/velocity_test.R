library(velocyto.R)

# if you want to run this, please download bam files (and genes.refFlat) from
# http://pklab.med.harvard.edu/velocyto/chromaffin/bams.tar
# and extract it ("tar xvf bams.tar") in the working directory.
# note: the file is fairly large - 5.6 GB! 
path <- "/Volumes/bonaguidi_lab/seq_post-process/bam/chrom_tst/e12.5.bams"
files <- system(paste('find',path,'-name "*unique.bam" -print'),intern=T)
names(files) <- gsub(".*\\/(.*)_unique.bam","\\1",files)
# parse gene annotation, annotate bam file reads
dat <- read.smartseq2.bams(files,"/Volumes/bonaguidi_lab/seq_post-process/bam/chrom_tst/genes.refFlat",n.cores=7)

if (F){
  

# post-processed data 
dat = readRDS("data/velocity_test_post-process/dat.rds")
cell.colors = readRDS("data/velocity_test_post-process/cell.colors.rds")
emb <- readRDS("data/velocity_test_post-process/embedding.rds")

################
#gene filtering#
################
#show histogram 
hist(log10(rowSums(dat$emat)+1),col='wheat',xlab='log10[ number of reads + 1]',main='number of reads per gene')

# exonic read (spliced) expression matrix
emat <- dat$emat;
# intronic read (unspliced) expression matrix
nmat <- dat$iomat;
# spanning read (intron+exon) expression matrix
smat <- dat$smat;
# filter expression matrices based on some minimum max-cluster averages
emat <- filter.genes.by.cluster.expression(emat,cell.colors,min.max.cluster.average = 5)
nmat <- filter.genes.by.cluster.expression(nmat,cell.colors,min.max.cluster.average = 1)
smat <- filter.genes.by.cluster.expression(smat,cell.colors,min.max.cluster.average = 0.5)
# look at the resulting gene set
resulting_gene_set = str(intersect(intersect(rownames(emat),rownames(nmat)),rownames(smat)))

#computes relative velocity esstimates 
#Using min/max quantile fit, in which case gene-specific offsets do not require spanning read (smat) fit. 
#Here the fit is based on the top/bottom 2% of cells (by spliced expression magnitude)

rvel.qf = gene.relative.velocity.estimates(emat,nmat,deltaT=1,kCells = 5,fit.quantile = 0.02)

#visualize velocities 
pca.velocity.plot(rvel.qf,nPcs=2,plot.cols=1,cell.colors=ac(cell.colors,alpha=0.7),cex=1.2,pcount=0.1)

# define custom pallet for expression magnitude for "Chga"
gene.relative.velocity.estimates(emat,nmat,deltaT=1,kCells = 5,fit.quantile = 0.02,old.fit=rvel.qf,show.gene='Chga',cell.emb=emb,cell.colors=cell.colors)

#same with "Serpine2"
gene.relative.velocity.estimates(emat,nmat,deltaT=1,kCells = 5,fit.quantile = 0.02,old.fit=rvel.qf,show.gene='Serpine2',cell.emb=emb,cell.colors=cell.colors)

#Alternatively, we calculate gene-relative velocity, using k=5 cell kNN pooling, 
#but now using entire range of expression to determine slope gamma, and using spanning reads (smat) to fit the gene offsets.
rvel <- gene.relative.velocity.estimates(emat,nmat,smat=smat,deltaT=1,kCells = 5, min.nmat.emat.slope = 0.1, min.nmat.smat.correlation = 0.1)

#visualize in PCA space 
pca.velocity.plot(rvel,nPcs=2,plot.cols=1,cell.colors=ac(cell.colors,alpha=0.7),cex=1.2,pcount=0.1)

#Velocity estimate based on gene structure 
# start with unfiltered matrices, as we can use more genes in these types of estimates
emat <- dat$emat; nmat <- dat$iomat; smat <- dat$smat;
emat <- filter.genes.by.cluster.expression(emat,cell.colors,min.max.cluster.average = 7)
gvel <- global.velcoity.estimates(emat, nmat, rvel, dat$base.df, smat=smat, deltaT=1, 
                                  kCells=5, kGenes = 15, kGenes.trim = 5, min.gene.cells = 0, min.gene.conuts = 500)

#plot in PCA space *after* global estimates 
pca.velocity.plot(gvel,nPcs=2,plot.cols=1,cell.colors=ac(cell.colors,alpha=0.7),cex=1.2,pcount=0.1)


#plot in PCA space *after* global estimates WITH vector field
pca.velocity.plot(gvel,nPcs=2,plot.cols=1,cell.colors=ac(cell.colors,alpha=0.7),cex=1.2,
                  pcount=0.1,show.grid.flow=TRUE,grid.arrow.scale = 1,min.grid.cell.mass=.5,grid.n=24,arrow.lwd=1.5)


#now on tSNE rvel
x <- tSNE.velocity.plot(rvel,nPcs=15,cell.colors=cell.colors,cex=0.9,perplexity=200,norm.nPcs=NA,pcount=0.1,scale='log',do.par=F)

#now on tSNE global 
x <- tSNE.velocity.plot(gvel,nPcs=15,cell.colors=cell.colors,cex=0.9,perplexity=200,norm.nPcs=NA,pcount=0.1,scale='log',do.par=F)

#visualization on existing embedding 
vel <- rvel; arrow.scale=6; cell.alpha=0.4; cell.cex=1; fig.height=4; fig.width=4.5;
show.velocity.on.embedding.cor(emb,vel,n=100,scale='sqrt',cell.colors=ac(cell.colors,alpha=cell.alpha),cex=cell.cex,arrow.scale=arrow.scale,arrow.lwd=1)

#vector field 
show.velocity.on.embedding.cor(emb,vel,n=100,scale='sqrt',cell.colors=ac(cell.colors,alpha=cell.alpha),
                               cex=cell.cex,arrow.scale=arrow.scale,show.grid.flow=TRUE,min.grid.cell.mass=1,grid.n=30,arrow.lwd=2)

#show trajectory 
x <- show.velocity.on.embedding.eu(emb,vel,n=40,scale='sqrt',cell.colors=ac(cell.colors,alpha=cell.alpha),
                                   cex=cell.cex,nPcs=30,sigma=2.5,show.trajectories=TRUE,diffusion.steps=500,
                                   n.trajectory.clusters=15,ntop.trajectories=1,embedding.knn=T,control.for.neighborhood.density=TRUE,n.cores=40) 
  

}