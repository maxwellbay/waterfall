#####################################################################################
#                                                                                   #
# GenHypDat (Generate Hypothetical Data)                                            #  
# Max Bay, September 2015                                                           #
#                                                                                   #
# Generates hypothetical dataset by changing the molecular identity of cells        #
# beyond threshold in pseudotime (PT) to have an identity that is closer to         #
# those of cells below threshold in PT.                                             #
#                                                                                   #
#####################################################################################

#Makes a list of cells which are conditionally >= threshold pseudotime value. 
GenHypDat.SelectCells = function(pt_data,pt_thresh=61,allocation_ratio=1.0){
  #pt_thresh is the threshold in pseudotime at which cells are considered for reallocation 
  #allocation_ratio is the ratio of above threhsold values that are reallocated to those above threhsold values which are not reallocated 
  
  print(c("Pseudotime Threshold Value: ",pt_thresh))
  print(c("Allocation Ratio          : ",allocation_ratio))
  
  cells_abovethresh_names = c() #initialize vector of names of cells at PT above threshold
  cell_names = row.names(pt_data) #makes vector of all cell names in same order as that from pt_data
  
  if (length(cell_names) == length(pt_data[,1])){ 
      
    for (celli in 1:length(cell_names)){ #loops through values between 1 and length(cell_names)
      cell_name = cell_names[celli] #instance val for the name of cell
      pt_value = pt_data[celli,1] #instance val for the PT of cell 
        
      if ((pt_value >= pt_thresh) & (pt_value < max(pt_data[,1])) ){ #conditonalizes on being >= pt_thresh
        cells_abovethresh_names = c(cells_abovethresh_names,cell_name) #adds cell name to vector
      }
    }
  }
  
  n_reallocated = round(length(cells_abovethresh_names)*allocation_ratio) #number of cells to be reallocated
  cells_abovethresh_names_reallocated = sample(cells_abovethresh_names,n_reallocated,replace=FALSE) #random sample of size n_reallocated from above threshold cells 

  return (cells_abovethresh_names_reallocated) # c(cell1,cell2,...,cellN) - vector of to-be-reallocated above threshold cells 

}

#Creates artificial transcriptome through interpolation 
GenHypDat.GenHypDat = function(tpm_data,pt_data, cells_to_reallocate,range_weight_mat){
  
  #tpm_data is the loaded tpm data
  #pt_data is the cell-pseudotime-dist_from_mst array
  #cells_to_reallocate returned by SelectCells
  #range_weight_mat is matrix of range for pseudotime-based interpolation [min,max,weight]
  
  MIN_INDEX = 1
  MAX_INDEX = 2
  WEIGHT_INDEX = 3
  
  
  for (row in 1:dim(range_weight_mat)[1]){ #itterates through rows of ranges to interpolate within  

    group_min = range_weight_mat[row,MIN_INDEX] #[MIN,max,weight]
    group_max = range_weight_mat[row,MAX_INDEX] #[min,MAX,weight]
    group_n = round(length(cells_to_reallocate)*range_weight_mat[row,WEIGHT_INDEX]) #[min,max,WEIGHT]
    
    if (row == 1){ #list indices for the start and end of cells_to_reallocate
      start_index = 1
      end_index = group_n
    } else{
      start_index = end_index + 1
      end_index = start_index + group_n 
    }
    
    new_pt_vals = runif(group_n,min=group_min,max=group_max) #vector of random 32 bit float vals between start and end of defined range in pseudotime, group_n is number defined by proportion of reallocated cells
    names(new_pt_vals) = cells_to_reallocate[start_index:end_index] #atomic vector c(new_pseudotime_value1,new_pseudotime_value2...)
    
    #This loop finds the two closest flanking pseudotime values to the new pseudotime value for each reallocated cell 
    #then interpolates a new transcriptome for that sell based on the flanking transcriptomes
    for (cell_name in names(new_pt_vals)){ #iterates through cell names of cells which are to be assigned a new transcriptome 
      new_pt_v = new_pt_vals[cell_name] #new, randomly prescribed pseudotime value 
      candidate_index = which.min(abs(jt.PT[,1]-new_pt_v)) #index of nearest extant pseudotime value 
      
      #The followings series of conditional statements finds flakning cells (in pseudotime) and computates interpolated transcriptome of interstitial cell
      if (pt_data[candidate_index,1]<new_pt_v){
        before_index = candidate_index
        
        if (pt_data[before_index,1]!=pt_data[before_index+1,1]){
          after_index = before_index + 1
          
        } else{
          for (dlta in 2:10){
            if (pt_data[before_index,1]< pt_data[before_index+dlta,1]){
              after_index = before_index + dlta
              break
            }
          }
        }
      }else{
        after_index = candidate_index 
        
        if(pt_data[after_index,1]!=pt_data[after_index - 1,1]){
          before_index = after_index - 1
         
        }else{
          for (dlta in 2:10){
            
            if (pt_data[before_index,1]< pt_data[before_index-dlta,1]){
              before_index = after_index - dlta
              break
                
            }
          }
        }
      }
      
      below_cell_name = row.names(pt_data)[before_index]
      above_cell_name = row.names(pt_data)[after_index]
      weight = (pt_data[after_index,1] - new_pt_v)/(pt_data[after_index,1]-pt_data[before_index,1])
      new_transcriptome = (tpm_data[below_cell_name]*weight)+(tpm_data[above_cell_name]*(1-weight))

      tpm_data[,cell_name] = new_transcriptome[,1]
      
      }
  }
  return (tpm_data) #        cell1, cell2, ..., celln  -  matrix of TPM values organized into genes x cells, reallocated cells have interpolated transcriptomes
                    # gene1, TPM11, TPM12, ..., TPM1n
                    # gene2, TPM21, TPM22, ..., TPM2n
                    # ...    ...  , ...  , ..., ...
                    # genem, ...  , ...  , ..., TPMnm
}