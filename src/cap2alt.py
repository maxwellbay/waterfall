import os
import csv 
import sys
import inspect

def importCSV(csv_path):
	gene_lst = []
	with open(csv_path,'Urw') as f:
		reader = csv.reader(f)
		for line in reader:
			gene_lst.append(line)
	
	return (gene_lst)
	
def doConversion(s):
	s = s[0] + s[1:len(s)].lower()
	return s
	
	
def exConversion(gene_lst,out_path):
	for i, line in enumerate(gene_lst):
		for j in range(1,len(line)):
			if len(str(line[j]).strip()) > 0:	
				gene_lst[i][j] = doConversion(gene_lst[i][j])
		
	print "Writing modified file to: {0}".format(out_path)		
	with open(out_path,'wr') as f:
		writer = csv.writer(f)
		writer.writerows(gene_lst)
				
	

def main():

	if len(sys.argv) == 3:
		csv_path = sys.argv[1]
		out_path = sys.argv[2]
	
		gene_lst = importCSV(csv_path)
		exConversion(gene_lst,out_path)
		
	else:
		print ("Must contain three args")
		print ("Usage: <basename> <in_path> <out_path>")
	
if __name__ == "__main__":
	main()
