# txt parser and reassignment 

renameGenes = function(mat,doc_path,srcID,dstID){
  conv = read.table(doc_path,header = TRUE)
  src = as.vector(conv[,srcID])
  dst = as.vector(conv[,dstID])
  names(dst) = src
  
  srcConf = intersect(rownames(mat),src)

  mat = mat[srcConf,] 
  
  rownames(mat) = dst[srcConf] 
  
  return(mat)
  
}

#example use
#tmp = jt[1:5,]
#rownames(tmp) = c("XLOC_000001","XLOC_000002","XLOC_000003","XLOC_000004","XLOC_000005")
#gn = renameGenes(tmp,"~/Desktop/genes.attr_table",1,5)