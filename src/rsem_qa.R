
rsemStatParser = function(cnt_file_path){
  cnts = read.table(cnt_file_path,fill = TRUE)
  cnts_labs = as.numeric(cnts[4:nrow(cnts),1])
  cnts_hist = as.numeric(cnts[4:nrow(cnts),2])
  
  if (any(cnts_labs==0)){
    unmapped = cnts_hist[which(cnts_labs==0)]
  }else{
    unmapped = 0
  }
  
  if (any(cnts_labs==1)){
    unique = cnts_hist[which(cnts_labs==1)]
  }else{
    unique = 0
  }
  
  if (any(cnts_labs>1)){
    ambig = sum(cnts_hist[which(cnts_labs>1)])
  }else{
    ambig = 0
  }

  
  reads = c(unique,ambig,unmapped)
  names(reads) = c("unique","ambiguous","unmapped")
  
  return(reads)
  
}

rsemSeriesStats = function(parent_dir,association_table=NULL){
  sample_fldrs = grep(list.dirs(parent_dir,full.names = FALSE),pattern = ".stat",value = TRUE)
  sample_nms = unlist(lapply(strsplit(sample_fldrs,"[.]"),function(l){return(l[1])}))
  
  types_nms = c("unique","ambiguous","unmapped")
  mapped_mat = matrix(0,nrow=3,ncol=length(sample_fldrs))
  rownames(mapped_mat) = types_nms
  colnames(mapped_mat) = sample_nms
  
  for (i in 1:length(sample_fldrs)){
    REG_EX = ".cnt"
    full_path = file.path(parent_dir,sample_fldrs[i],paste0(sample_nms[i],REG_EX))
    if (file.exists(full_path)){
      stats = rsemStatParser(full_path)
    }else{
      stats = c(0,0,0)
      names(stats) = types_nms
    }
    mapped_mat[types_nms,i] = stats[types_nms]
    
  }
  
  total_reads = apply(mapped_mat,2,sum)
  mapped_mat = mapped_mat[,order(total_reads)]
  if(!is.null(association_table)) colnames(mapped_mat) = association_table[colnames(mapped_mat)]

  return(mapped_mat)
  
}

plotSeriesStats = function(mapped_reads_mat,main="Mapping QA"){
  map_cols = c("#FAF7F3","#69E563","#F03B10")
  names(map_cols) = c(rownames(mapped_reads_mat))
  
  prop = prop.table(mapped_reads_mat,margin = 2)
  b=barplot(mapped_reads_mat, col=map_cols,cex.names=.0000001, las=1,space=.5,main = main)
  text(b, max(apply(mapped_reads_mat,2,sum))*-.005, 
       srt = 60, adj= 1, xpd = TRUE,
       labels = colnames(mapped_reads_mat), cex=0.35)
  
  legend("topleft", fill=map_cols, legend=names(map_cols),box.lwd = 0)
  
  
}

plotCompPie = function(mapped_reads_mat,main = "Global Read Composition"){
  map_cols = c("#FAF7F3","#69E563","#F03B10")
  
  mapped = apply(mapped_reads_mat,1,sum)
  pct = round(mapped/sum(mapped)*100,2)
  lbls = paste(names(mapped), pct) # add percents to labels 
  lbls = paste(lbls,"%",sep="") # ad % to labels 
  pie(mapped,labels = lbls, col=map_cols,
      main=main)
  
}



