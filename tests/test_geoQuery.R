library("GEOquery")
library("SRAdb")
library("dbConnect")

GSE = "GSE71485"
SRP = "SRP061746"

PREFETCH = "/shared/seq/software/sratoolkit.2.9.0-ubuntu64/bin/prefetch"
FQ_DUMP = "/shared/seq/software/sratoolkit.2.9.0-ubuntu64/bin/fastq-dump"

OUTPUT_PARENT = "/shared/seq/data"

gse =  getGEO(GSE, GSEMatrix = TRUE)

filePaths = getGEOSuppFiles(GSE)

pdat = pData(gse[[1]])

project_title = sub(",,","_",pdat$contact_name[1])
parent_dir = file.path(OUTPUT_PARENT,project_title)

if (!dir.exists(parent_dir)) system(paste("mkdir -p",parent_dir))

SRAmetadb_dir = "data"
SRAmetadb_name = "SRAmetadb.sqlite"
SRAmetadb_gz = paste0(SRAmetadb_name,".gz")

full_SRAdb_path = file.path(SRAmetadb_dir,SRAmetadb_name)

if (!dir.exists(SRAmetadb_dir)) system(paste("mkdir -p",SRAmetadb_dir))
if (!file.exists(full_SRAdb_path)) sqlfile = getSRAdbFile(destdir = SRAmetadb_dir,SRAmetadb_gz)

con = dbConnect(RSQLite::SQLite(),full_SRAdb_path)

sra_dat = listSRAfile(SRP,con)


srx_unsorted = unlist(lapply(strsplit(as.character(pdat$relation.1),"="),function(l){l[2]}))
cls_unsorted = as.character(pdat$title)

sra_dat$cls = cls_unsorted[match(sra_dat$experiment,srx_unsorted)]
rownames(sra_dat) = sra_dat$run

srr = sra_dat$run

if (F){
  for (i in 1:length(srr)){
    
    srr_inst = srr[i]
    
    system(paste(PREFETCH,srr_inst))
    
    translated_name = paste0(srr_inst,".fastq")
    daughter_path = file.path(parent_dir,translated_name)
    
    system(paste(FQ_DUMP,srr_inst,"--gzip","--split-files","-O",daughter_path))
    
  }
  
}

